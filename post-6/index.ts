import { logProfile }             from './src/sample-commands/LogProfile'
import { logSomeExampleCommands } from './src/sample-commands/LogSomeExampleCommands'
import { runWikiSample }          from './src/sample-commands/RunWikiSample'
import { session }                from './src/session/Session'

module.exports = {
    login :                  session.getWebId () ,
    logout :                 session.logout ,
    logSomeExampleCommands : logSomeExampleCommands ,
    logProfile :             logProfile ,
    runWikiSampleCommands :  runWikiSample
}

session.getWebId ()
       .then ( ( webId ) => {
           if ( webId ) {
               session.renderLogin ( webId )
           }
       } )

session.getWebId ()
       .then ( ( webId ) => {
           if ( webId ) {
               session.renderLogin ( webId )
           }
       } )


document.addEventListener (
    'DOMContentLoaded' ,
    async function ( event ) {
    }
)



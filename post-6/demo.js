
await SolidWiki.api.addNewPage("https://alice.solid.ma.parrillo.eu/public/demo/wiki/pages", "My first wiki page", "Hello there.");

await SolidWiki.api.addNewPage("https://alice.solid.ma.parrillo.eu/public/another/folder", "A second page", "This page is persisted somewhere else....");


await SolidWiki.api.recursivelyLoadAllWikis(SolidWiki.api.wiki);

await SolidWiki.api.logAllPages(SolidWiki.api.wikis);


await SolidWiki.api.createNewWiki("https://alice.solid.ma.parrillo.eu/public/second", "Alice second wiki", "public");

await SolidWiki.api.addNewPage("https://alice.solid.ma.parrillo.eu/public/random/pages", "This is cool", "Hello there.");


await SolidWiki.api.recursivelyLoadAllWikis(SolidWiki.api.wiki);
await SolidWiki.api.logAllPages(SolidWiki.api.wikis);

await SolidWiki.api.addWikiToSource(SolidWiki.api.wiki, "https://alice.solid.ma.parrillo.eu/public/demo/wiki/wiki.ttl");


SolidWiki.api.wikis;

await SolidWiki.api.setProfile();
await SolidWiki.api.createNewWiki("https://alice.solid.ma.parrillo.eu/public/second", "Alice second wiki", "public");
await SolidWiki.api.recursivelyLoadAllWikis(SolidWiki.api.wiki);
await SolidWiki.api.logAllPages(SolidWiki.api.wikis);


wiki = SolidWiki.api.wiki;
SolidWiki.api.wikiService.getAllPageChangeNotifications(wiki);

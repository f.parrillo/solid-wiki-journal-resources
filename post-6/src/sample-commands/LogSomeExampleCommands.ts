import { session } from '../session/Session'

// @ts-ignore
const RDF   = $rdf.Namespace ( 'http://www.w3.org/1999/02/22-rdf-syntax-ns#' )
const DCT   = $rdf.Namespace ( 'http://purl.org/dc/terms/' )
const VCARD = $rdf.Namespace ( 'http://www.w3.org/2006/vcard/ns#' )
const SIOC  = $rdf.Namespace ( 'http://rdfs.org/sioc/ns#' )

export const logSomeExampleCommands = async () => {
    const webId = await session.getWebId ()
    if ( webId ) {
        const store   = $rdf.graph ()
        const fetcher = new $rdf.Fetcher ( store )

        await fetcher.load ( webId )
        const me = store.sym ( webId )

        // @ts-ignore
        const profile = me.doc ()
        console.log ( profile )

        let note       = $rdf.sym ( 'https://parrillo.inrupt.net/public/notes.ttl' )  // NOTE: Ends in a slash
        const response = await fetcher.load ( note )

        // @ts-ignore
        console.log ( response.responseText )

        console.log ( 'any' )
        const name = store.any (
            me ,
            $rdf.sym ( 'http://xmlns.com/foaf/0.1/name' ) ,
            null ,
            // @ts-ignore
            me.doc ()
        )
        console.log ( name )

        console.log ( 'match' )
        let text = store.match (
            null ,
            $rdf.sym ( 'http://schema.org/text' ) ,
            null ,
            null
        )
        console.log ( text )

        console.log ( 'each' )
        // @ts-ignore
        text
            = store.each (
            null ,
            $rdf.sym ( 'http://schema.org/text' )
        )
        console.log ( text )

    }
}



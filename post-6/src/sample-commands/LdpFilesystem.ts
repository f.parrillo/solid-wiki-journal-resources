import {
    ldp ,
    rdf
}                         from 'rdf-namespaces'
import { IndexedFormula } from 'rdflib'
import {
    NamedNode ,
    Quad_Object
}                         from 'rdflib/lib/tf-types'
import { session }        from '../session/Session'

type Mode =
    'recursive'
    | 'normal'

export const deleteResource = async (
    resource : NamedNode ,
    mode : Mode = 'normal'
) => {
    const webId = await session.getWebId ()
    if ( webId ) {
        const store   = $rdf.graph ()
        const fetcher = new $rdf.Fetcher ( store )
        await fetcher.load ( resource )
        switch ( mode ) {
            case 'normal':
                await store.fetcher?.webOperation (
                    'DELETE' ,
                    resource
                )
                return
            case 'recursive':
                await deleteRecursive ( resource )
                return
        }
    }

    throw new Error ( 'User is not logged in' )
}

const traverseDepthFirst = async ( folder : NamedNode ) : Promise<Quad_Object[]> => {
    const fileQueue : Quad_Object[] = []
    fileQueue.push ( folder )

    const store   = $rdf.graph ()
    const fetcher = new $rdf.Fetcher ( store )
    await fetcher.load ( folder )

    const recursiveTraverseDepthFirst = async (
        folder : NamedNode ,
        store : IndexedFormula
    ) => {
        const filesOrFolders = store.match (
            folder ,
            $rdf.sym ( ldp.contains ) ,
            null
        )
                                    .map ( s => s.object )

        for ( let fileOrFolder of
            filesOrFolders ) {
            const isFolder = store.holds (
                fileOrFolder ,
                $rdf.sym ( rdf.type ) ,
                $rdf.sym ( ldp.BasicContainer )
            )
            if ( !isFolder ) {
                // Push file to delete queue
                fileQueue.push ( fileOrFolder )
                continue
            }

            fileQueue.push ( fileOrFolder )

            const nextFolder = new $rdf.NamedNode ( fileOrFolder.value )
            await store.fetcher?.load ( nextFolder )
            await recursiveTraverseDepthFirst (
                nextFolder ,
                store
            )
        }
    }

    await recursiveTraverseDepthFirst (
        folder ,
        store
    )
    return fileQueue
}

const deleteRecursive = async ( folder : NamedNode ) => {
    const deleteQueue = await traverseDepthFirst ( folder )

    const store   = $rdf.graph ()
    const fetcher = new $rdf.Fetcher ( store )
    while ( deleteQueue.length !== 0 ) {
        // @ts-ignore
        const next : NamedNode = new $rdf.NamedNode ( deleteQueue.pop ().value )
        await fetcher.webOperation (
            'DELETE' ,
            next
        )
    }
}

const listFiles = async (
    resource : NamedNode ,
    mode : Mode
) : Promise<string[]> => {
    const fileList = await traverseDepthFirst ( resource )
    return fileList.map ( q => q.value )
}

const createFolder = async ( resource : NamedNode ) : Promise<void> => {
    console.log ( `Create folder ${ resource.value }` )
    const store   = $rdf.graph ()
    const fetcher = new $rdf.Fetcher ( store )
    const dummy   = new $rdf.NamedNode ( resource.value.endsWith ( '/' )
                                         ? `${ resource.value }.dummy`
                                         : `${ resource.value }/.dummy` )
    await fetcher.webOperation (
        'PUT' ,
        dummy
    )
    await fetcher.webOperation (
        'DELETE' ,
        dummy
    )
}


type LdpFilesystem = {
    delete : (
        resource : NamedNode ,
        mode : Mode
    ) => Promise<void>
    listFiles : (
        resource : NamedNode ,
        mode : Mode
    ) => Promise<string[]>
    createFolder : ( PUBLIC_TEST_FOLDER : NamedNode ) => Promise<void>
}


export const LdpFilesystem : LdpFilesystem = {
    delete :       deleteResource ,
    listFiles :    listFiles ,
    createFolder : createFolder
}

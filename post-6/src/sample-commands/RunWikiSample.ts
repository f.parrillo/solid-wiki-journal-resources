import { ProfileService }    from 'domain/profile/ProfileService'
import { SourceListService } from 'domain/wiki/config/source-list/SourceListService'
import { PageService }       from 'domain/wiki/page/PageService'
import { PageIndexService }  from 'domain/wiki/pageIndex/PageIndexService'
import { WikiService }       from 'domain/wiki/WikiService'
import { session }           from '../session/Session'

export const runWikiSample = async () => {

    const profileService    = new ProfileService ()
    const wikiService       = new WikiService ()
    const sourceListService = new SourceListService ()
    const pageService       = new PageService ()
    const pageIndexService  = new PageIndexService ()

    const webid = await session.getWebId ()
    if ( !webid ) {
        throw new Error ( 'Log in and make sure to call set profile before you use the api.' )
    }

    const profile = await profileService.getProfile ( webid )
    let wikis     = await wikiService.getAllWikisByProfile ( profile )
    console.log ( `## Number of Wikis ${ JSON.stringify ( wikis.length ) }` )
    confirm ( 'Create a wiki first wiki.' )
    const createdWiki1 = await wikiService.createBlank
                                          (
                                              profile ,
                                              'https://alice.solid.ma.parrillo.eu/public/wiki1' ,
                                              'First WIKI'
                                          )
    console.log ( `## Created new wiki with title: ${ await createdWiki1.getTitle () }` )

    confirm ( 'Create a page.' )
    const page1 = await pageService.create
                                   (
                                       {
                                           title :     'First Page' ,
                                           body :      'This is my first wiki entry....' ,
                                           container : 'https://alice.solid.ma.parrillo.eu/public/wiki1/pages'
                                       }
                                   )

    confirm ( 'Add page to the first wiki.' )

    await wikiService.addPageToWiki (
        profile ,
        createdWiki1 ,
        page1
    )

    confirm ( 'Create a second wiki.' )
    const createdWiki2 = await wikiService.createBlank
                                          (
                                              profile ,
                                              'https://alice.solid.ma.parrillo.eu/public/wiki2' ,
                                              'Second WIKI'
                                          )
    console.log ( `## Created new wiki with title: ${ await createdWiki2.getTitle () }` )

    confirm ( 'Add page to a second page.' )
    const page2 = await pageService.create
                                   (
                                       {
                                           title :     'First page of second Wiki' ,
                                           body :      'Blaaa....' ,
                                           container : 'https://alice.solid.ma.parrillo.eu/public/wiki2/pages'
                                       }
                                   )

    confirm ( 'Add the second page to the second wiki.' )

    await wikiService.addPageToWiki
                     (
                         profile ,
                         createdWiki2 ,
                         page2
                     )

    confirm ( 'Add wiki2 as a source to wiki1' )
    await sourceListService.addSourceByWiki
                           (
                               createdWiki1 ,
                               createdWiki2
                           )

    confirm ( 'Load all sources of wiki 1' )
    const allWikisOfWiki1 = await wikiService.loadAllSourceWikis ( createdWiki1 )
    console.log ( `## ${ await createdWiki1.getTitle () } contains ${ allWikisOfWiki1.length }` )

    console.log ( '############################################################' )
    for ( let w of allWikisOfWiki1 ) {
        console.log ( `## ${ await w.getTitle () }` )
        for ( let p of await pageIndexService.getPages ( w ) ) {
            console.log ( `## Page Title: ${ await p.getTitle () }` )
            console.log ( `##      Body : ${ await p.getBody () }` )
        }
    }

    confirm ( 'Load all sources of wiki 2' )
    const allWikisOfWiki2 = await wikiService.loadAllSourceWikis ( createdWiki2 )
    console.log ( `## ${ await createdWiki2.getTitle () } contains ${ allWikisOfWiki2.length }` )
    console.log ( '############################################################' )
    for ( let w of allWikisOfWiki2 ) {
        console.log ( `## ${ await w.getTitle () }` )
        for ( let p of await pageIndexService.getPages ( w ) ) {
            console.log ( `## Page Title: ${ await p.getTitle () }` )
            console.log ( `##      Body : ${ await p.getBody () }` )
        }
    }


    confirm ( 'Wiki1 subscribe to wiki2' )
    await wikiService.forWiki ( createdWiki1 )
                     .subscribeTo ( createdWiki2 )


    confirm ( 'Create wiki3' )
    const createdWiki3 = await wikiService.createBlank
                                          (
                                              profile ,
                                              'https://alice.solid.ma.parrillo.eu/public/wiki3' ,
                                              '3. WIKI'
                                          )
    console.log ( `## Created new wiki with title: ${ await createdWiki3.getTitle () }` )

    confirm ( 'Wiki1 subscribe to wiki3' )
    await wikiService.forWiki ( createdWiki1 )
                     .subscribeTo ( createdWiki3 )


    confirm ( 'Create page 3.' )
    const page3 = await pageService.create
                                   (
                                       {
                                           title :     'WOoooo' ,
                                           body :      'Blaaa....' ,
                                           container : 'https://alice.solid.ma.parrillo.eu/public/wiki2/pages'
                                       }
                                   )

    confirm ( 'Add page 3 to wiki2.' )
    await wikiService.addPageToWiki
                     (
                         profile ,
                         createdWiki2 ,
                         page3
                     )


    confirm ( 'Sync page inbox of wiki 1.' )
    await wikiService.syncPageInbox ( createdWiki1 )

    const inbox         = await createdWiki1.getInbox ()
    const notifications = inbox.getNotifications ()
    console.log ( notifications )

    confirm ( 'Add wiki3 as a source to wiki1' )
    await sourceListService.addSourceByWiki
                           (
                               createdWiki3 ,
                               createdWiki1
                           )


    confirm ( 'Load all sources of wiki 3' )
    const allWikisOfWiki3 = await wikiService.loadAllSourceWikis ( createdWiki3 )
    console.log ( `## ${ await createdWiki3.getTitle () } contains ${ allWikisOfWiki3.length }` )
    console.log ( '############################################################' )
    for ( let w of allWikisOfWiki3 ) {
        console.log ( `## ${ await w.getTitle () }` )
        for ( let p of await pageIndexService.getPages ( w ) ) {
            console.log ( `## Page Title: ${ await p.getTitle () }` )
            console.log ( `##      Body : ${ await p.getBody () }` )
        }
    }
}

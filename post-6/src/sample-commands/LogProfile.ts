import { session } from '../session/Session'

export const logProfile = async () => {
    const webId = await session.getWebId ()
    if ( webId ) {
        const store   = $rdf.graph ()
        const fetcher = new $rdf.Fetcher ( store )
        await fetcher.load ( $rdf.sym ( webId ) )
        $rdf.serialize (
            $rdf.sym ( webId )
                // @ts-ignore
                .doc () ,
            store ,
            undefined ,
            'text/turtle' ,
            ( (
                err ,
                result
            ) => {
                console.log ( result )
            } )
        )
    }
}



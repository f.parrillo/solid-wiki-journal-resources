type Property = string;
type Class = string;
type Datatype = string;
type OwlClass = string;
type OwlObjectProperty = string;
type OwlDatatypeProperty = string;


const Wiki : Class         = 'http://parrillo.eu/ns/solid/wiki#Wiki'
const Page : Class         = 'http://parrillo.eu/ns/solid/wiki#Page'
const Body : Class         = 'http://parrillo.eu/ns/solid/wiki#Body'
const Title : Class        = 'http://parrillo.eu/ns/solid/wiki#Title'
const SourceList : Class   = 'http://parrillo.eu/ns/solid/wiki#SourceList'
const Config : Class       = 'http://parrillo.eu/ns/solid/wiki#Config'
const PageIndex : Class    = 'http://parrillo.eu/ns/solid/wiki#PageIndex'
const Subscription : Class = 'http://parrillo.eu/ns/solid/wiki#Subscription'
const Feed : Class         = 'http://parrillo.eu/ns/solid/wiki#Feed'

const page : Property         = 'http://parrillo.eu/ns/solid/wiki#page'
const body : Property         = 'http://parrillo.eu/ns/solid/wiki#body'
const title : Property        = 'http://parrillo.eu/ns/solid/wiki#title'
const source : Property       = 'http://parrillo.eu/ns/solid/wiki#source'
const config : Property       = 'http://parrillo.eu/ns/solid/wiki#config'
const sourceList : Property   = 'http://parrillo.eu/ns/solid/wiki#sourceList'
const pageIndex : Property    = 'http://parrillo.eu/ns/solid/wiki#pageIndex'
const subscription : Property = 'http://parrillo.eu/ns/solid/wiki#subscription'
const feed : Property         = 'http://parrillo.eu/ns/solid/wiki#feed'

export const wiki = {
    Wiki , Page , Body , Title , SourceList , Config , PageIndex ,
    Subscription , Feed ,

    page , body , title , source , config , sourceList , pageIndex ,
    subscription , feed


}

type Property = string;
type Class = string;
type Datatype = string;
type OwlClass = string;
type OwlObjectProperty = string;
type OwlDatatypeProperty = string;


const Actor : Class  = 'https://www.w3.org/ns/activitystreams#Actor'
const Target : Class = 'https://www.w3.org/ns/activitystreams#Target'

const actor : Property   = 'https://www.w3.org/ns/activitystreams#actor'
const target : Property  = 'https://www.w3.org/ns/activitystreams#target'
const updated : Property = 'https://www.w3.org/ns/activitystreams#updated'

export const activitystreams = {
    Actor , Target ,
    actor , target ,
    updated
}

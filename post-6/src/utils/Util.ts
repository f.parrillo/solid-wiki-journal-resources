import { namedNode }  from '@rdfjs/data-model'
import {
    Path ,
    PathFactory
}                     from 'ldflex'
import ComunicaEngine from 'ldflex-comunica'
import auth           from 'solid-auth-client'

export async function spqrqlUpdate (
    uri : string ,
    query : string
) : Promise<Response> {
    return await auth.fetch (
        uri ,
        {
            method :  'POST' ,
            headers : new Headers ( { 'Content-Type' : 'application/sparql-update' } ) ,
            body :    query
        }
    )
}


export async function httpDelete ( uri : string ) : Promise<Response> {
    return await auth.fetch (
        uri ,
        { method : 'DELETE' }
    )
}

export async function httpGET ( uri : string ) : Promise<Response> {
    return await auth.fetch (
        uri ,
        { method : 'GET' }
    )
}


export async function getHexHash ( message : string ) {
    const encoder        = new TextEncoder ()
    const uint8Array     = encoder.encode ( message )
    const hashUnit8Array = new Uint8Array ( await crypto.subtle.digest (
        'SHA-256' ,
        uint8Array
    ) )

    // https://gist.github.com/tauzen/3d18825ae41ff3fc8981
    let hexHash = ''
    for (
        let i = 0 ;
        i < hashUnit8Array.length ;
        i++
    ) {
        let hex = ( hashUnit8Array[ i ] & 0xff ).toString ( 16 )
        hexHash
            += hex
    }
    return hexHash
}


export function asPath (
    uri : string ,
    context : any ,
    assureMainIsMainReference : boolean = true
) : Path {
    if ( assureMainIsMainReference && !uri.endsWith ( '#' ) ) {
        uri
            += '#'
    }
    const queryEngine = new ComunicaEngine ( uri )
    const path        = new PathFactory ( { context , queryEngine } )
    return path.create ( { subject : namedNode ( uri ) } )
}

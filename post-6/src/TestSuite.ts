import { ProfileService }    from 'domain/profile/ProfileService'
import { TypedIndexService } from 'domain/solid/terms/typed-index/TypedIndexService'
import { session }           from './session/Session'

const testWikiContext = async () => {
    let webId = await session.getWebId ()
    if ( webId ) {

        const profileService = new ProfileService ()
        const profile        = await profileService.getProfile ( webId )
        const allTypedIndex  = await profileService.getAllTypedIndexes ( profile )

        const typedIndexService = new TypedIndexService ()
        /*

                const pages = await typedIndexService.getAllByTypeFromTypedIndexes<Page>(allTypedIndex, Page);
                console.log(pages);

                const sourceList = await typedIndexService.getAllByTypeFromTypedIndexes<SourceList>(allTypedIndex, SourceList);
                console.log(sourceList) // will find no entries and return []

        */

        // const wikiService = new WikiService();
        //
        // const wiki = await wikiService.create(profile, "https://alice.solid.ma.parrillo.eu/public/test2", "TEst wiki2", 'private');
        //
        /*
                const pageService = new PageService();
                const page = await pageService.create(
                    {
                        container: "https://alice.solid.ma.parrillo.eu/public/test",
                        title: "This-is-a-test-title",
                        body: "This is a test body1."
                    }
                );
                await wikiService.addPageToWiki(wiki, page);

                const sourceListService = new SourceListService();
                await sourceListService.addSourceByWiki(wiki, "https://other.test");
                await wikiService.addPageToWiki(wiki, page)
        */
        // const wikis = await (await (await (await wiki.getConfig()).getSourceList())).getWikis()
        // console.log(wikis)
        // add other wiki to source list
        // load all wiki pages

        /*
                const profile = new Profile(data[webId]);
                const indexes = await profile.publicTypedIndex();
                const typeRegistrations: TypeRegistration[] = await indexes[1].getTypeRegistrations();
                const a = await resolve<Page>(typeRegistrations, Page);
                console.log(a)
        */
        /*
                const p2 = await pageService.get("https://alice.solid.ma.parrillo.eu/public/test/This-is-a-test-title/page.ttl");
                console.log((await p2.getTitle()).toString());
        */
        /*
                await pageService.delete("https://alice.solid.ma.parrillo.eu/public/test/This-is-a-test-title/page.ttl");
        */

    }
}

export const TestSuite = {
    testLdpFilesystem :  () => {
    } ,
    testLdpWikiContext : testWikiContext
}

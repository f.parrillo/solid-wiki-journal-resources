import { Profile }           from 'domain/profile/Profile'
import { ProfileRepository } from 'domain/profile/ProfileRepository'
import { TypedIndex }        from 'domain/solid/terms/typed-index/TypedIndex'
import { getTagger }         from '../../utils/logger'

export class ProfileService {
    private profieRepositroy : ProfileRepository
    protected tag = getTagger ( ProfileService.name ).tag

    constructor () {
        this.profieRepositroy = new ProfileRepository ()
    }

    async getProfile ( webId : string ) : Promise<Profile> {
        console.debug ( this.tag ( `Get profile with WebId ${ webId }` ) )
        return await this.profieRepositroy.get ( webId )
    }

    async getAllTypedIndexes ( profile : Profile ) : Promise<TypedIndex[]> {
        console.debug ( this.tag ( `Get all typed indexes for ${ profile }` ) )
        return await this.profieRepositroy.getAllTypedIndexes ( profile )
    }

}

import { Node }              from 'domain/rdf/Node'
import { PrivateTypedIndex } from 'domain/solid/terms/typed-index/PrivateTypedIndex'
import { PublicTypedIndex }  from 'domain/solid/terms/typed-index/PublicTypedIndex'
import {
    Path ,
    ResolvedPath
}                            from 'ldflex'
import { solid }             from 'rdf-namespaces'

export class Profile extends Node {
    constructor ( profile : Path ) {
        super ( profile )
    }

    async name () : Promise<ResolvedPath> {
        return this.getPath ().name
    }

    async setName ( name : string ) : Promise<ResolvedPath> {
        return this.getPath ()
                   .name
                   .set ( name )
    }

    async friends () : Promise<Profile[]> {
        return this.getPath ()
                   .friend
                   .toArray ()
                   .then ( friends =>
                               friends.map ( f => new Profile ( f ) )
                   )
    }

    async publicTypedIndex () : Promise<PublicTypedIndex[]> {
        return this.getPath ()[ solid.publicTypeIndex ]
            .toArray ()
            .then ( indexes => indexes.map ( i => new PublicTypedIndex ( i ) ) )
    }

    async privateTypedIndex () : Promise<PrivateTypedIndex[]> {
        return this.getPath ()[ solid.privateTypeIndex ]
            .toArray ()
            .then ( indexs => indexs.map ( i => new PrivateTypedIndex ( ( i ) ) ) )
    }

}


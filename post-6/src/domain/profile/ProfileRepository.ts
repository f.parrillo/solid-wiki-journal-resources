import data                 from '@solid/query-ldflex'
import { Profile }          from 'domain/profile/Profile'
import { TypeRegistration } from 'domain/solid/terms/typed-index/type-registraion/TypeRegistration'
import { TypedIndex }       from 'domain/solid/terms/typed-index/TypedIndex'
import { getTagger }        from '../../utils/logger'
import { Get }              from '../interfaces/ICreate'


export class ProfileRepository
    implements Get<Profile> {
    protected tag = getTagger ( ProfileRepository.name ).tag

    async get ( uri : string ) : Promise<Profile> {
        console.debug ( this.tag ( `Get profile with uri ${ uri }` ) )
        return new Profile ( data[ uri ] )
    }

    async getAllTypedIndexes ( profile : Profile ) : Promise<TypedIndex[]> {
        console.debug ( this.tag ( `Get all type indexes for ${ profile }` ) )
        const typedIndexes : TypedIndex[] = []
        for ( let i of await profile.privateTypedIndex () ) {
            typedIndexes.push ( i )
        }

        for ( let i of await profile.publicTypedIndex () ) {
            typedIndexes.push ( i )
        }

        console.debug ( this.tag ( `Found ${ typedIndexes.length } type index(es).` ) )
        return typedIndexes
    }

    async getAllTypeRegistrations ( profile : Profile ) : Promise<TypeRegistration[]> {
        console.debug ( this.tag ( `Get all type registrations for ${ profile }` ) )
        const registrations : TypeRegistration[] = []
        for ( let index of await this.getAllTypedIndexes ( profile ) ) {
            for ( let reg of await index.getTypeRegistrations () ) {
                registrations.push ( reg )
            }
        }

        console.debug ( this.tag ( `Found ${ registrations.length } type registration(s).` ) )
        return registrations
    }

}

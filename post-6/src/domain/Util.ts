import data                 from '@solid/query-ldflex'
import { TypeRegistration } from 'domain/solid/terms/typed-index/type-registraion/TypeRegistration'
import {
    Path ,
    ResolvedPath
}                           from 'ldflex'

export const assertAsReferencePath = async ( proxy : Path ) : Promise<ResolvedPath> => {
    let predicate = await proxy
    console.log ( proxy )
    console.log ( predicate )
    console.log ( predicate.toString () )
    if ( predicate.toString ()
                  .endsWith ( '.ttl' ) ) {
        const instanceReference = `${ predicate }#`
        return data[ instanceReference ]
    }
    return proxy
}

type _class<T> = {
    type : string;
    new ( proxy : Path ) : T
}


export const resolve = async <T> (
    typeRegistrations : TypeRegistration[] ,
    _class : _class<T>
) : Promise<T[]> => {
    const result : T[] = []
    for await ( let t of
        typeRegistrations ) {
        let instances     = await t.getAllTypedIncenses ()
        let type : string = ( await t.forClass () ).toString ()
        for await ( let i of
            instances ) {
            console.log ( i )
            console.log ( type )
            console.log ( _class.type )
            if ( type === _class.type ) {
                result.push ( new _class ( i.getPath () ) )
            }
        }
    }
    return result
}

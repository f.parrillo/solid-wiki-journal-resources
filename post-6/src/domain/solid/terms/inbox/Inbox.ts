import { Container }    from 'domain/linked-data-platform/Container'
import { Notification } from 'domain/solid/terms/notification/notification'
import { Path }         from 'ldflex'
import { solid }        from 'rdf-namespaces'
import { getTagger }    from '../../../../utils/logger'

export class Inbox extends Container {
    static type : string = solid.Inbox
    protected tag        = getTagger ( Inbox.name ).tag

    constructor ( inbox : Path ) {
        super ( inbox )
    }

    async getNotifications () : Promise<Notification[]> {
        const resources                     = await this.getTurtleResources ()
        const notification : Notification[] = []
        for ( let r of resources ) {
            notification.push ( new Notification ( await r.getPath () ) )
        }
        return notification
    }
}

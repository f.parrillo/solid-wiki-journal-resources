import { Node }             from 'domain/rdf/Node'
import { TurtleDocument }   from 'domain/solid/terms/typed-index/TurtleDocument'
import { TypeRegistration } from 'domain/solid/terms/typed-index/type-registraion/TypeRegistration'
import { Path }             from 'ldflex'

export class TypedIndex extends Node {
    private document : TurtleDocument
    private cachedInstances : any[] | undefined

    constructor ( listedDocument : Path ) {
        super ( listedDocument )
        this.document
            = new TurtleDocument ( this.path )
    }


    async getTypeRegistrations () : Promise<TypeRegistration[]> {
        console.log ( `Get all instances for on  ${ this.toString () } ` )
        return ( await this.document.getSubjects () ).map ( i => new TypeRegistration ( i ) )
    }
}

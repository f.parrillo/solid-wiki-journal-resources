import data                  from '@solid/query-ldflex'
import { PrivateTypedIndex } from 'domain/solid/terms/typed-index/PrivateTypedIndex'
import { PublicTypedIndex }  from 'domain/solid/terms/typed-index/PublicTypedIndex'
import { TypeRegistration }  from 'domain/solid/terms/typed-index/type-registraion/TypeRegistration'
import {
    ldp ,
    solid
}                            from 'rdf-namespaces'
import {
    getHexHash ,
    spqrqlUpdate
}                            from '../../../../utils/Util'

export class PrivateTypedIndexRepository {

    async createBlank ( uri : string ) : Promise<PrivateTypedIndex> {
        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <> a 
                                            <${ PublicTypedIndex.type }> , 
                                            <${ ldp.Resource }> 
                        ;
                        .
                    }`
        )
        return new PublicTypedIndex ( await data[ uri ] )
    }

    async addNewEntry (
        privateTypedIndex : PrivateTypedIndex ,
        instanceUri : string ,
        typeUri : string
    ) {
        const id = await getHexHash ( instanceUri )
        await spqrqlUpdate (
            privateTypedIndex.getPath ()
                             .toString () ,
            `INSERT DATA {
                        <${ id }> a                     <${ TypeRegistration.type }> ; 
                                 <${ solid.forClass }>   <${ typeUri }> ; 
                                 <${ solid.instance }>   <${ instanceUri }> ; 
                        .
                    }`
        )
    }
}

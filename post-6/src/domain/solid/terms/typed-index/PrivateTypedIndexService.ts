import { PrivateTypedIndex }           from 'domain/solid/terms/typed-index/PrivateTypedIndex'
import { PrivateTypedIndexRepository } from 'domain/solid/terms/typed-index/PrivateTypedIndexRepository'
import { PageIndex }                   from 'domain/wiki/pageIndex/PageIndex'

export class PrivateTypedIndexService {

    constructor (
        private privateTypedIndexRepository = new PrivateTypedIndexRepository ()
    ) {
    }

    async createBlank ( uri : string ) : Promise<PrivateTypedIndex> {
        return this.privateTypedIndexRepository.createBlank ( uri )
    }

    async addNewEntry (
        pageIndex : PageIndex ,
        instanceUri : string ,
        typeUri : string
    ) {
        await this.privateTypedIndexRepository.addNewEntry (
            pageIndex ,
            instanceUri ,
            typeUri
        )
    }
}

import { Node }              from 'domain/rdf/Node'
import { Instance }          from 'domain/solid/terms/typed-index/type-registraion/instance/Instance'
import { InstanceContainer } from 'domain/solid/terms/typed-index/type-registraion/instance/InstanceContainer'
import { Path }              from 'ldflex'
import { solid }             from 'rdf-namespaces'

export class TypeRegistration extends Node {
    static type : string = solid.TypeRegistration

    constructor ( typeRegistration : Path ) {
        super ( typeRegistration )
    }

    async forClass () : Promise<Path> {
        return this.path[ solid.forClass ]
    }

    async getInstance () : Promise<Instance> {
        let instance : Path = await this.path[ solid.instance ]
        if ( !instance ) {
            throw new Error ( `${ this.path } has no property ${ solid.instance }` )
        }
        return new Instance ( instance )
    }

    async getInstanceContainer () : Promise<InstanceContainer> {
        return this.path[ solid.instanceContainer ]
            .then ( i => {
                if ( i ) {
                    return i
                }
                else {
                    throw new Error ( `${ this.path } has no property ${ solid.instanceContainer }` )
                }
            } )
            .then ( ic => new InstanceContainer ( ic ) )
    }

    async getAllTypedIncenses () : Promise<Instance[]> {
        const allInstances : Instance[] = []
        try {
            const instance = await this.getInstance ()
            allInstances.push ( instance )
        }
        catch ( e1 ) {
            try {
                const instanceContainer = await this.getInstanceContainer ()
                const all               = await instanceContainer.getAllInstances ()
                for ( const instance of
                    all ) {
                    allInstances.push ( instance )
                }
            }
            catch ( e2 ) {
                // Skip this instance, possible causes:
                //   - instance not retrievable
                //   - neither instance or instance uri present
                console.warn ( e1.message )
                console.warn ( e2.message )
            }
        }
        return allInstances
    }

}

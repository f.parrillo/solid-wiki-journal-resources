import { Node } from 'domain/rdf/Node'
import { Path } from 'ldflex'

export class Instance extends Node {

    constructor ( instance : Path ) {
        super ( instance )
    }
}

import { Node }     from 'domain/rdf/Node'
import { Instance } from 'domain/solid/terms/typed-index/type-registraion/instance/Instance'
import { Path }     from 'ldflex'

export class InstanceContainer extends Node {

    constructor ( instanceContainer : Path ) {
        super ( instanceContainer )
    }


    // @ts-ignore
    async getAllInstances () : Promise<Instance[]> {
        // todo recursively got through all containers and return the getTypeRegistrations
        // it could be the uri contains a instance of a other class
        // therefore consider to pass the type/class downwards to insure only
        // instance of the correct type are relieved
        // otherwise we would have to check it later on, with a high partial to
        // runtime errorc

        // todo do LdpFilesystem because of rdflib.js
        /*
                const all = await LdpFilesystem.listFiles(new $rdf.NamedNode(this.instanceContainer.toString()), 'recursive')
                const getTypeRegistrations = all.filter(i => i.endsWith(".ttl"))
                const result: Path[] = []
                for (const uri of getTypeRegistrations) {
                    const next: Path = await data[uri];
                    result.push(new Instance(next))
                }
                return result
        */
    }
}

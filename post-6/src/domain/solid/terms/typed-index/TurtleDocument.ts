import { Node } from 'domain/rdf/Node'
import {
    Path ,
    ResolvedPath
}               from 'ldflex'

export class TurtleDocument extends Node {
    constructor ( turtleDocument : Path ) {
        super ( turtleDocument )
    }

    async getSubject () : Promise<Path> {
        return this.filterDocumentItself ( this.path.subjects.toArray () )
    }

    async getSubjects () : Promise<Path[]> {
        return this.filterOutDocumentItself ( this.path.subjects.toArray () )
    }

    private async filterDocumentItself ( subjects : Promise<Path[]> ) : Promise<ResolvedPath> {
        return ( await subjects )
            .filter ( s => s.toString () === `${ this.path.toString () }` )
            .pop ()
    }

    private async filterOutDocumentItself ( subjects : Promise<Path[]> ) : Promise<ResolvedPath[]> {
        return ( await subjects )
            .filter ( s => s.toString () !== `${ this.path.toString () }` )
    }

}

import data                 from '@solid/query-ldflex'
import { TypeRegistration } from 'domain/solid/terms/typed-index/type-registraion/TypeRegistration'
import { TypedIndex }       from 'domain/solid/terms/typed-index/TypedIndex'
import { Path }             from 'ldflex'


export type _class<T> = {
    type : string;
    new ( proxy : Path ) : T
}

export class TypedIndexRepository {

    async getAll<T> (
        typeRegistrations : TypeRegistration[] ,
        _class : _class<T>
    ) {
        const result : T[] = []
        for await ( let t of
            typeRegistrations ) {
            try {
                console.debug ( `Type registration ${ t.getPath () }` )
                let instances = await t.getAllTypedIncenses ()
                if ( instances.length === 0 ) {
                    continue
                }

                let type : string = ( await t.forClass () ).toString ()
                for await ( let i of
                    instances ) {
                    try {
                        if ( type === _class.type ) {
                            console.debug ( `Add instance ${ i.getPath () } of type ${ type } to result list` )
                            result.push ( new _class ( data[ i.getPath ()
                                                              .toString () ] ) )
                        }
                    }
                    catch ( e ) {
                        console.warn ( e.message )
                    }
                }

            }
            catch ( e ) {
                console.warn ( e.message )
            }
        }
        return result
    }


    async getAllTypeRegistrations ( typedIndexes : TypedIndex[] ) : Promise<TypeRegistration[]> {
        const all : TypeRegistration[] = []
        for ( let i of
            typedIndexes ) {
            for ( let r of
                await i.getTypeRegistrations () ) {
                all.push ( r )
            }
        }
        return all
    }

    async getAllByTypeFromTypedIndexes<T> (
        typedIndexes : TypedIndex[] ,
        _class : _class<T>
    ) : Promise<T[]> {
        return this.getAll (
            await this.getAllTypeRegistrations ( typedIndexes ) ,
            _class
        )
    }
}


import { Instance }   from 'domain/solid/terms/typed-index/type-registraion/instance/Instance'
import { TypedIndex } from 'domain/solid/terms/typed-index/TypedIndex'
import {
    _class ,
    TypedIndexRepository
}                     from 'domain/solid/terms/typed-index/TypedIndexRepository'


export class TypedIndexService {
    private typedIndexRepository : TypedIndexRepository

    constructor () {
        this.typedIndexRepository
            = new TypedIndexRepository ()
    }

    async add (
        index : TypedIndex ,
        instance : Instance ,
        type : string
    ) {

    }

    async getAllByTypeFromTypedIndexes<T> (
        typedIndexes : TypedIndex[] ,
        _class : _class<T>
    ) : Promise<T[]> {
        return this.typedIndexRepository.getAllByTypeFromTypedIndexes<T> (
            typedIndexes ,
            _class
        )
    }

}

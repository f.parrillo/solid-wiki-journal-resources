import data                 from '@solid/query-ldflex'
import { PublicTypedIndex } from 'domain/solid/terms/typed-index/PublicTypedIndex'
import { TypeRegistration } from 'domain/solid/terms/typed-index/type-registraion/TypeRegistration'
import {
    ldp ,
    solid
}                           from 'rdf-namespaces'
import { getTagger }        from '../../../../utils/logger'
import {
    getHexHash ,
    spqrqlUpdate
}                           from '../../../../utils/Util'

export class PublicTypedIndexRepository {
    protected tag = getTagger ( PublicTypedIndexRepository.name ).tag

    async createBlank ( uri : string ) : Promise<PublicTypedIndex> {
        console.debug ( this.tag ( `Create blank public typed index ${ uri }` ) )
        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <> a 
                                            <${ PublicTypedIndex.type }> , 
                                            <${ ldp.Resource }> 
                        ;
                        .
                    }`
        )
        return new PublicTypedIndex ( await data[ uri ] )
    }

    async addNewEntry (
        publicTypedIndex : PublicTypedIndex ,
        instanceUri : string ,
        typeUri : string
    ) {
        console.debug ( this.tag ( `Add new entry to  public typed index ${ publicTypedIndex }, instance: ${ instanceUri }, type: ${ typeUri }` ) )
        const id = await getHexHash ( instanceUri )
        await spqrqlUpdate (
            publicTypedIndex.getPath ()
                            .toString () ,
            `INSERT DATA {
                        :${ id } a                     <${ TypeRegistration.type }> ; 
                                 <${ solid.forClass }>   <${ typeUri }> ; 
                                 <${ solid.instance }>   <${ instanceUri }> ; 
                        .
                    }`
        )
    }
}

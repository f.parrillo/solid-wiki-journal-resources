import { PublicTypedIndex }           from 'domain/solid/terms/typed-index/PublicTypedIndex'
import { PublicTypedIndexRepository } from 'domain/solid/terms/typed-index/PublicTypedIndexRepository'
import { PageIndex }                  from 'domain/wiki/pageIndex/PageIndex'

export class PublicTypedIndexService {

    constructor (
        private publicTypedIndexRepository = new PublicTypedIndexRepository ()
    ) {
    }

    async createBlank ( uri : string ) : Promise<PublicTypedIndex> {
        return this.publicTypedIndexRepository.createBlank ( uri )
    }

    async addNewEntry (
        pageIndex : PageIndex ,
        instanceUri : string ,
        typeUri : string
    ) {
        await this.publicTypedIndexRepository.addNewEntry (
            pageIndex ,
            instanceUri ,
            typeUri
        )
    }
}

import data                from '@solid/query-ldflex'
import { TurtleResource }  from 'domain/iana/media-types/text/turtle/TurtleResource'
import { Notification }    from 'domain/solid/terms/notification/notification'
import { PageFeedInbox }   from 'domain/wiki/page-feed/inbox/PageFeedInbox'
import { Path }            from 'ldflex'
import { activitystreams } from '../../../../namespace/ActivityStreams'
import { getTagger }       from '../../../../utils/logger'
import { spqrqlUpdate }    from '../../../../utils/Util'


export class NotificationRepository {
    protected tag = getTagger ( NotificationRepository.name ).tag

    async addAll (
        inbox : PageFeedInbox ,
        notifications : Notification[]
    ) {
        console.log ( this.tag ( `Add ${ notifications.length } notifications to ${ inbox.getPath () }` ) )
        for ( let n of notifications ) {
            await this.create (
                inbox.getPath ()
                     .toString () + n.getResourceName () ,
                await n.getActor () ,
                await n.getUpdated () ,
                await n.getTarget () ,
            )
        }
    }


    async create (
        uri : string ,
        actor : string ,
        date : Date ,
        target : Path
    ) : Promise<Notification> {
        console.debug ( this.tag ( `Create notification uri: ${ uri }, actor: ${ actor }` ) )
        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <>  a                           <${ Notification.type }> ,
                                                        <${ TurtleResource.type }>;   
                            <${ activitystreams.actor }>        <${ actor }> ; 
                            <${ activitystreams.updated }>      "${ date.toISOString () }"; 
                            <${ activitystreams.target }>       <${ target }> ;
                        .
                    }`
        )
        return new Notification ( await data[ uri ] )
    }
}

import { TurtleResource }  from 'domain/iana/media-types/text/turtle/TurtleResource'
import { Path }            from 'ldflex'
import { solid }           from 'rdf-namespaces'
import { activitystreams } from '../../../../namespace/ActivityStreams'

export class Notification extends TurtleResource {
    public static type : string = solid.Notification

    constructor ( notification : Path ) {
        super ( notification )
    }

    async getActor () : Promise<string> {
        return ( await this.path[ activitystreams.actor ] ).toString ()
    }

    async getUpdated () : Promise<Date> {
        return new Date ( ( await this.path[ activitystreams.updated ] ).toString () )
    }

    async getTarget () : Promise<string> {
        return ( await this.path[ activitystreams.target ] ).toString ()
    }

}

import data              from '@solid/query-ldflex'
import { Notification }  from 'domain/solid/terms/notification/notification'
import { PageFeedInbox } from 'domain/wiki/page-feed/inbox/PageFeedInbox'
import { ldp }           from 'rdf-namespaces'
import { getTagger }     from '../../../../utils/logger'
import {
    httpDelete ,
    spqrqlUpdate
}                        from '../../../../utils/Util'

export class PageFeedInboxRepository {
    protected tag = getTagger ( PageFeedInboxRepository.name ).tag

    async createBlank ( uri : string ) : Promise<PageFeedInbox> {
        console.debug ( this.tag ( `Create blank page feed inbox ${ uri }` ) )

        await spqrqlUpdate ( uri + '.dummy' ,
                             `INSERT DATA {   }` )
        await httpDelete ( uri + '.dummy' )

        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <> a 
                                            <${ PageFeedInbox.type }> , 
                                            <${ ldp.Resource }>,
                                            <${ ldp.Container }> 
                        ;
                        .
                    }`
        )
        return new PageFeedInbox ( await data[ uri ] )

    }

    async getAllNotifications ( inbox : PageFeedInbox ) : Promise<Notification[]> {
        console.debug ( this.tag ( `Get all notifications for page feed inbox ${ inbox }` ) )

        const notifications = await inbox.getNotifications ()
                                         .then ( notifications => notifications.map ( n => new Notification ( n.getPath () ) ) )

        console.debug ( this.tag ( `Retrieved  ${ notifications.length } notifications.` ) )
        return notifications

    }

    async getNewNotifications (
        inbox : PageFeedInbox ,
        potentialNotifications : Notification[]
    ) {
        console.debug ( this.tag ( 'Invoked get new notifications.' ) )
        const inboxNotification                 = await inbox.getNotifications ()
        const newNotifications : Notification[] = []

        console.debug (
            this.tag (
                `Seek for now notifications. Inbox contains ${ inboxNotification.length } notifications,` +
                `there are ${ potentialNotifications.length } potential new notification.`
            ) )
        for ( const pN of potentialNotifications ) {
            let isNew = true

            for ( const iN of inboxNotification ) {
                if ( pN.getResourceName () === iN.getResourceName () ) {
                    isNew = false
                    break
                }
            }
            if ( isNew ) {
                console.debug ( this.tag ( `Found new notification ${ pN.getResourceName () }` ) )
                newNotifications.push ( pN )
            }
        }
        console.debug ( this.tag ( `Found ${ newNotifications.length } new notification ` ) )
        return newNotifications
    }
}

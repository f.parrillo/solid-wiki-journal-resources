import { Notification }            from 'domain/solid/terms/notification/notification'
import { PageFeedInbox }           from 'domain/wiki/page-feed/inbox/PageFeedInbox'
import { PageFeedInboxRepository } from 'domain/wiki/page-feed/inbox/PageFeedInboxRepository'
import { getTagger }               from '../../../../utils/logger'

export class PageFeedInboxService {
    protected tag = getTagger ( PageFeedInboxService.name ).tag

    constructor (
        private pageChangeInboxRepository = new PageFeedInboxRepository () ,
    ) {
    }

    async createBlank ( uri : string ) : Promise<PageFeedInbox> {
        console.debug ( this.tag ( `Create blank page feed inbox ${ uri }` ) )
        return this.pageChangeInboxRepository.createBlank ( uri )
    }

    async getAllNotification ( inbox : PageFeedInbox ) : Promise<Notification[]> {
        console.debug ( this.tag ( `Get all notifications page feed inbox ${ inbox }` ) )
        return await this.pageChangeInboxRepository.getAllNotifications ( inbox )
    }
}

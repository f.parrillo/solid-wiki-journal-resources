import { Inbox } from 'domain/solid/terms/inbox/Inbox'
import { Path }  from 'ldflex'

export class PageFeedInbox extends Inbox {

    constructor ( pageChangeInbox : Path ) {
        super ( pageChangeInbox )
    }


}

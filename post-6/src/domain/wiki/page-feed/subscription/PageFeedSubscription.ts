import { Node }      from 'domain/rdf/Node'
import { PageFeed }  from 'domain/wiki/page-feed/feed/PageFeed'
import { Path }      from 'ldflex'
import { wiki }      from '../../../../namespace/Wiki'
import { getTagger } from '../../../../utils/logger'

export class PageFeedSubscription extends Node {
    public static type : string = wiki.Subscription
    protected tag               = getTagger ( PageFeedSubscription.name ).tag

    constructor ( pageChangeInbox : Path ) {
        super ( pageChangeInbox )
    }

    async getPageFeeds () : Promise<PageFeed[]> {
        console.debug ( this.tag ( `Get page feeds.` ) )
        const feeds : PageFeed[] = await this.getPath ()[ wiki.subscription ]
            .toArray ()
            .then ( feeds => feeds.map ( f => new PageFeed ( f ) ) )

        return this.filterExistingNodes<PageFeed> ( feeds )
    }

}

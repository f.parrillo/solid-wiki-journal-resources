import data                     from '@solid/query-ldflex'
import { Notification }         from 'domain/solid/terms/notification/notification'
import { PageFeed }             from 'domain/wiki/page-feed/feed/PageFeed'
import { PageFeedSubscription } from 'domain/wiki/page-feed/subscription/PageFeedSubscription'
import { ldp }                  from 'rdf-namespaces'
import { wiki }                 from '../../../../namespace/Wiki'
import { getTagger }            from '../../../../utils/logger'
import { spqrqlUpdate }         from '../../../../utils/Util'

export class PageFeedSubscriptionsRepository {
    private tag = getTagger ( PageFeedSubscriptionsRepository.name ).tag

    /**
     *
     * @param uri without tailing slash (ex. https://alice.solid.org/newInbox
     */
    async createBlank ( uri : string ) : Promise<PageFeedSubscription> {

        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <> a 
                                            <${ PageFeedSubscription.type }> , 
                                            <${ ldp.Resource }>
                        ;
                        .
                    }`
        )
        return new PageFeedSubscription ( await data[ uri ] )
    }

    async addFeedToSubscriptions (
        subscriptions : PageFeedSubscription ,
        pageFeed : PageFeed
    ) : Promise<void> {
        await subscriptions.getPath ()[ wiki.subscription ].add ( pageFeed.getPath () )
    }


    async pullNotifications ( subscriptions : PageFeedSubscription[] ) {
        console.debug ( this.tag ( `Pull notifications from ${ subscriptions.length } subscription(s).` ) )
        const notifications : Notification[] = []
        for ( let s of subscriptions ) {
            for ( let f of await s.getPageFeeds () ) {
                for await ( let n of await f.getNotifications () ) {
                    notifications.push ( n )
                }
            }
        }

        console.debug ( this.tag ( `Found ${ notifications.length } notifications.` ) )
        return notifications
    }
}

import data          from '@solid/query-ldflex'
import { PageFeed }  from 'domain/wiki/page-feed/feed/PageFeed'
import { ldp }       from 'rdf-namespaces'
import { getTagger } from '../../../../utils/logger'
import {
    httpDelete ,
    spqrqlUpdate
}                    from '../../../../utils/Util'

export class PageFeedRepository {
    protected tag = getTagger ( PageFeedRepository.name ).tag

    async createBlank ( uri : string ) : Promise<PageFeed> {
        await spqrqlUpdate ( uri + '.dummy' ,
                             `INSERT DATA {   }` )
        await httpDelete ( uri + '.dummy' )

        // 08:40:54.095 XHRPOST https://example.eu/public/wiki3/wiki/page-feed-inbox/
        // [HTTP/1.1 500 Internal Server Error 27ms]
        //
        // Answer
        // Original file read error: Error: EISDIR: illegal operation on a directory, read
        // ToDo does not insert triplets to turtle folder
        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <> a 
                                            <${ PageFeed.type }> , 
                                            <${ ldp.Resource }>,
                                            <${ ldp.Container }> 
                        ;
                        .
                    }`
        )
        return new PageFeed ( await data[ uri ] )
    }

}

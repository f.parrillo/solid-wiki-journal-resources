import { Profile }                from 'domain/profile/Profile'
import { NotificationRepository } from 'domain/solid/terms/notification/NotificationRepository'
import { PageFeed }               from 'domain/wiki/page-feed/feed/PageFeed'
import { PageFeedRepository }     from 'domain/wiki/page-feed/feed/PageFeedRepository'
import { Path }                   from 'ldflex'
import { getTagger }              from '../../../../utils/logger'
import { getHexHash }             from '../../../../utils/Util'

export class PageFeedService {
    protected tag = getTagger ( PageFeedService.name ).tag

    constructor (
        private pageFeedRepository : PageFeedRepository         = new PageFeedRepository () ,
        private notificationRepository : NotificationRepository = new NotificationRepository () ,
    ) {
    }

    async createBlank ( uri : string ) : Promise<PageFeed> {
        console.debug ( this.tag ( `Create blank page feed ${ uri }` ) )
        return this.pageFeedRepository.createBlank ( uri )
    }

    async addNewNotification (
        pageFeed : PageFeed ,
        actorProfile : Profile ,
        target : Path
    ) : Promise<void> {
        console.debug ( this.tag ( `Add new notification to page feed: ${ pageFeed }, actor: ${ actorProfile }, target: ${ target }` ) )

        const actor    = actorProfile.getPath ()
                                     .toString ()
        const date     = new Date ()
        const fileName = await getHexHash ( `${ target }${ actor }${ date.toISOString () }` )
        const uri      = `${ pageFeed.getPath ()
                                     .toString () }/${ fileName }.ttl`
        await this.notificationRepository
                  .create (
                      uri ,
                      actor ,
                      date ,
                      target
                  )
    }
}

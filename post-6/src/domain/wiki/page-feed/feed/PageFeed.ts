import { Container }    from 'domain/linked-data-platform/Container'
import { Notification } from 'domain/solid/terms/notification/notification'
import { Path }         from 'ldflex'
import { getTagger }    from '../../../../utils/logger'

export class PageFeed extends Container {
    protected tag = getTagger ( PageFeed.name ).tag
    constructor ( pageChangeNotification : Path ) {
        super ( pageChangeNotification )
    }

    async getNotifications () : Promise<Notification[]> {
        // todo could use a generic type for getResources<T>()
        return ( await this.getResources () )
            .map ( t => {
                return new Notification ( t.getPath () )
            } )
    }
}

import { Profile }                         from 'domain/profile/Profile'
import { ProfileRepository }               from 'domain/profile/ProfileRepository'
import { Notification }                    from 'domain/solid/terms/notification/notification'
import { NotificationRepository }          from 'domain/solid/terms/notification/NotificationRepository'
import { PublicTypedIndexRepository }      from 'domain/solid/terms/typed-index/PublicTypedIndexRepository'
import { TypedIndexRepository }            from 'domain/solid/terms/typed-index/TypedIndexRepository'
import { ConfigRepository }                from 'domain/wiki/config/ConfigRepository'
import { SourceListRepository }            from 'domain/wiki/config/source-list/SourceListRepository'
import { PageFeedRepository }              from 'domain/wiki/page-feed/feed/PageFeedRepository'
import { PageFeedInboxRepository }         from 'domain/wiki/page-feed/inbox/PageFeedInboxRepository'
import { PageFeedSubscriptionsRepository } from 'domain/wiki/page-feed/subscription/PageFeedSubscriptionRepository'
import { Page }                            from 'domain/wiki/page/Page'
import { PageIndexRepository }             from 'domain/wiki/pageIndex/PageIndexRepository'
import { Wiki }                            from 'domain/wiki/Wiki'
import {
    WikiCreateArgs ,
    WikiRepository
}                                          from 'domain/wiki/WikiRepository'
import { getTagger }                       from '../../utils/logger'
import { getHexHash }                      from '../../utils/Util'

export class WikiService {
    protected tag = getTagger ( WikiService.name ).tag

    constructor (
        private wikiRepository : WikiRepository                                   = new WikiRepository () ,
        private sourceListRepository : SourceListRepository                       = new SourceListRepository () ,
        private configRepository : ConfigRepository                               = new ConfigRepository () ,
        private pageIndexRepository : PageIndexRepository                         = new PageIndexRepository () ,
        private publicTypedIndexRepository : PublicTypedIndexRepository           = new PublicTypedIndexRepository () ,
        private pageFeedInboxRepository : PageFeedInboxRepository                 = new PageFeedInboxRepository () ,
        private typedIndexRepository : TypedIndexRepository                       = new TypedIndexRepository () ,
        private profileRepository : ProfileRepository                             = new ProfileRepository () ,
        private pageFeedRepository : PageFeedRepository                           = new PageFeedRepository () ,
        private pageFeedSubscriptionsRepository : PageFeedSubscriptionsRepository = new PageFeedSubscriptionsRepository () ,
        private notificationRepository : NotificationRepository                   = new NotificationRepository () ,
    ) {
    }


    async create ( wiki : WikiCreateArgs ) : Promise<Wiki> {
        console.log ( this.tag ( `Create page ${ wiki.title } in ${ wiki.uri }` ) )
        return this.wikiRepository.create ( wiki )
    }

    async createBlank (
        user : Profile ,
        baseUri : string ,
        title : string ,
        mode : 'private' | 'public' = 'private'
    ) : Promise<Wiki> {
        console.log ( this.tag ( `Create blank wiki.  user: ${ user }, base uri: ${ baseUri }, title: ${ title }, mode: ${ mode }` ) )

        const sourceList = await this.sourceListRepository.createBlank (
            `${ baseUri }/wiki/config/source-list.ttl`
        )

        const config = await this.configRepository.createBlank ( {
                                                                     uri :        `${ baseUri }/wiki/config/config.ttl` ,
                                                                     sourceList : sourceList
                                                                 } )

        const pageIndex = await this.pageIndexRepository.createBlank (
            `${ baseUri }/wiki/page-index.ttl`
        )

        const inbox = await this.pageFeedInboxRepository.createBlank (
            `${ baseUri }/wiki/page-feed-inbox/`
        )

        const feed = await this.pageFeedRepository.createBlank (
            `${ baseUri }/wiki/page-feed/`
        )

        const subscription = await this.pageFeedSubscriptionsRepository.createBlank (
            `${ baseUri }/wiki/subscriptions.ttl`
        )
        const wiki         = await this.create (
            {
                uri :          `${ baseUri }/wiki/wiki.ttl` ,
                pageIndex :    pageIndex ,
                config :       config ,
                title :        title ,
                inbox :        inbox ,
                feed :         feed ,
                subsciptions : subscription

            }
        )

        if ( mode === 'public' ) {
            // todo add a profile service and get the default public and private typed index, there could also be an separate private index ony for this application
            await this.publicTypedIndexRepository.addNewEntry (
                // @ts-ignore
                ( await user.publicTypedIndex () ).pop () ,
                wiki.getPath ()
                    .toString () ,
                Wiki.type
            )
        }
        else {
            await this.publicTypedIndexRepository.addNewEntry (
                // @ts-ignore
                ( await user.privateTypedIndex () ).pop () ,
                wiki.getPath ()
                    .toString () ,
                Wiki.type
            )
        }
        return wiki
    }

    async addPageToWiki (
        profile : Profile ,
        wiki : Wiki ,
        page : Page ,
    ) {
        await this.pageIndexRepository.addNewEntry (
            await wiki.getPageIndex () ,
            page.getPath ()
                .toString () ,
            Page.type
        )

        const actor    = profile.getPath ()
                                .toString ()
        const date     = new Date ()
        const target   = page.getPath ()
        const fileName = await getHexHash ( `${ target }${ actor }${ date.toISOString () }` )
        const uri      = `${ ( await wiki.getPageFeed () ).getPath ()
                                                          .toString () }/${ fileName }.ttl`
        await this.notificationRepository
                  .create (
                      uri ,
                      actor ,
                      date ,
                      target
                  )

    }

    async loadAllSourceWikis ( wiki : Wiki ) : Promise<Wiki[]> {
        const sources : { [ uri : string ] : Wiki } = {}
        sources[ wiki.getPath ()
                     .toString () ]
                                                    = wiki
        const recursiveLookup = async ( next : Wiki ) => {
            try {
                const config     = await next.getConfig ()
                const sourceList = await config.getSourceList ()
                const nextWikis  = await sourceList.getWikis ()

                for ( let w of
                    nextWikis ) {
                    let key = w.getPath ()
                               .toString ()
                    if ( sources[ key ] === undefined ) {
                        sources[ key ]
                            = w
                    }
                    await recursiveLookup ( w )
                }
            }
            catch ( e ) {
                console.warn ( `Something went wrong while loading sources of ${ wiki.getPath ()
                                                                                     .toString () }` )
                console.warn ( `${ e.name } ${ e.message }` )
            }
        }
        await recursiveLookup ( wiki )
        return Object.values ( sources )
    }

    async getAllPageChangeNotifications ( wiki : Wiki ) : Promise<Notification[]> {
        return this.pageFeedInboxRepository.getAllNotifications ( await wiki.getInbox () )
    }

    async getWiki ( uri : string ) : Promise<Wiki> {
        return await this.wikiRepository.get ( uri )
    }

    async getAllWikisByProfile ( profile : Profile ) {
        return this.typedIndexRepository.getAll<Wiki> (
            await this.profileRepository.getAllTypeRegistrations ( profile ) ,
            Wiki
        )

    }

    forWiki ( wiki : Wiki ) : {
        subscribeTo : ( wiki : Wiki ) => Promise<void>
    } {
        return {
            subscribeTo : async ( to : Wiki ) => {
                await this.pageFeedSubscriptionsRepository
                          .addFeedToSubscriptions (
                              await wiki.getSubscription () ,
                              await to.getPageFeed ()
                          )
            }
        }
    }


    async syncPageInbox ( wiki : Wiki ) {
        const subscriptions          = await wiki.getSubscriptions ()
        const potentialNotifications = await this.pageFeedSubscriptionsRepository
                                                 .pullNotifications ( subscriptions )
        const inbox                  = await wiki.getInbox ()
        const newNotifications       = await this.pageFeedInboxRepository.getNewNotifications (
            inbox ,
            potentialNotifications
        )

        await this.notificationRepository.addAll (
            inbox ,
            newNotifications
        )
    }
}


import { Node }                 from 'domain/rdf/Node'
import { Config }               from 'domain/wiki/Index'
import { PageFeed }             from 'domain/wiki/page-feed/feed/PageFeed'
import { PageFeedInbox }        from 'domain/wiki/page-feed/inbox/PageFeedInbox'
import { PageFeedSubscription } from 'domain/wiki/page-feed/subscription/PageFeedSubscription'
import { PageIndex }            from 'domain/wiki/pageIndex/PageIndex'
import { Title }                from 'domain/wiki/title/Title'
import { Path }                 from 'ldflex'
import { solid }                from 'rdf-namespaces'
import { wiki }                 from '../../namespace/Wiki'
import { getTagger }            from '../../utils/logger'

export class Wiki extends Node {
    static type : string         = wiki.Wiki
    private title : Title | null = null
    protected tag                = getTagger ( Wiki.name ).tag

    constructor ( wiki : Path ) {
        super ( wiki )
    }

    async getTitle () : Promise<Title> {
        console.debug ( this.tag ( `Get title of ${ this }` ) )
        const p : Path = this.path[ wiki.title ]
        if ( !this.title ) {
            this.title
                = new Title (
                p ,
                await p.value
            )
        }
        return this.title
    }

    async setTitle ( title : string ) : Promise<void> {
        console.debug ( this.tag ( `Set title ${ title } of ${ this }` ) )
        let t = await this.getTitle ()
        await t.setTitle ( title )
    }

    async getConfig () : Promise<Config> {
        console.debug ( this.tag ( `Get config of ${ this }` ) )
        return new Config ( await this.path[ wiki.config ] )
    }

    async getPageIndex () : Promise<PageIndex> {
        console.debug ( this.tag ( `Get page index ${ this }` ) )
        return new PageIndex ( await this.path[ wiki.pageIndex ] )
    }

    /**
     * Returns the inbox containing all the page change notification
     * of the subscribed Wiki's.
     */
    async getInbox () : Promise<PageFeedInbox> {
        console.debug ( this.tag ( `Get inbox ${ this }` ) )
        return new PageFeedInbox ( await this.path[ solid.inbox ] )
    }

    /**
     * Returns all page change subsciptions of a Wiki.
     */
    async getSubscriptions () : Promise<PageFeedSubscription[]> {
        console.debug ( this.tag ( `Get page feed  subscriptions ${ this }` ) )
        return ( await this.path[ wiki.subscription ].toArray () )
            .map ( s => new PageFeedSubscription ( s ) )
    }

    async getSubscription () : Promise<PageFeedSubscription> {
        console.debug ( this.tag ( `Get page feed subscription ${ this }` ) )
        return new PageFeedSubscription ( await this.path[ wiki.subscription ] )
    }


    async getPageFeed () : Promise<PageFeed> {
        console.debug ( this.tag ( `Get page feed ${ this }` ) )
        return new PageFeed ( await this.path[ wiki.feed ] )
    }

}

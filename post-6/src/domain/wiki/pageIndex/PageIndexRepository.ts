import data                 from '@solid/query-ldflex'
import { TypeRegistration } from 'domain/solid/terms/typed-index/type-registraion/TypeRegistration'
import { PageIndex }        from 'domain/wiki/pageIndex/PageIndex'
import {
    ldp ,
    solid
}                           from 'rdf-namespaces'
import { getTagger }        from '../../../utils/logger'
import {
    getHexHash ,
    spqrqlUpdate
}                           from '../../../utils/Util'

export class PageIndexRepository {
    protected tag = getTagger ( PageIndexRepository.name ).tag

    async createBlank ( uri : string ) : Promise<PageIndex> {
        console.debug ( this.tag ( `Create blank page index ${ uri } ` ) )
        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <> a 
                                            <${ PageIndex.type }> , 
                                            <${ ldp.Resource }> 
                        ;
                        .
                    }`
        )
        return new PageIndex ( await data[ uri ] )
    }

    async addNewEntry (
        pageIndex : PageIndex ,
        instanceUri : string ,
        typeUri : string
    ) {
        console.debug ( this.tag ( `Add new entry to page index ${ pageIndex }, instance: ${ instanceUri }, type: ${ typeUri } ` ) )
        const id = await getHexHash ( instanceUri )
        await spqrqlUpdate (
            pageIndex.getPath ()
                     .toString () ,
            `INSERT DATA {
                        :${ id } a                     <${ TypeRegistration.type }> ; 
                                 <${ solid.forClass }>   <${ typeUri }> ; 
                                 <${ solid.instance }>   <${ instanceUri }> ; 
                                  
                        .
                    }`
        )
    }
}

import { TypedIndexService }   from 'domain/solid/terms/typed-index/TypedIndexService'
import { Page }                from 'domain/wiki/page/Page'
import { PageIndex }           from 'domain/wiki/pageIndex/PageIndex'
import { PageIndexRepository } from 'domain/wiki/pageIndex/PageIndexRepository'
import { Wiki }                from 'domain/wiki/Wiki'
import { getTagger }           from '../../../utils/logger'

export class PageIndexService {
    protected tag = getTagger ( PageIndexService.name ).tag

    constructor (
        private pageIndexRepository = new PageIndexRepository () ,
        private typedIndexService   = new TypedIndexService ()
    ) {
    }

    async createBlank ( uri : string ) : Promise<PageIndex> {
        console.debug ( this.tag ( `Create blank page index ${ uri }` ) )
        return this.pageIndexRepository.createBlank ( uri )
    }

    async addNewEntry (
        pageIndex : PageIndex ,
        instanceUri : string ,
        typeUri : string
    ) {
        console.debug ( this.tag ( `Add new entry to page index ${ pageIndex }, instance: ${ instanceUri }, type: ${ typeUri } ` ) )
        await this.pageIndexRepository.addNewEntry (
            pageIndex ,
            instanceUri ,
            typeUri
        )
    }

    async getPages ( wiki : Wiki ) : Promise<Page[]> {
        console.debug ( this.tag ( `Get wiki pages.` ) )
        return await this.typedIndexService.getAllByTypeFromTypedIndexes<Page> (
            [ await wiki.getPageIndex () ] ,
            Page
        )

    }
}

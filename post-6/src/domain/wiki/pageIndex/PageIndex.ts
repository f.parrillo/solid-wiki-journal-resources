import { TypedIndex } from 'domain/solid/terms/typed-index/TypedIndex'
import { Path }       from 'ldflex'

export class PageIndex extends TypedIndex {
    constructor ( typedIndex : Path ) {
        super ( typedIndex )
    }

}

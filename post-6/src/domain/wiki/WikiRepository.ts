import data                     from '@solid/query-ldflex'
import {
    Get ,
    ICreate
}                               from 'domain/interfaces/ICreate'
import { Config }               from 'domain/wiki/Index'
import { PageFeed }             from 'domain/wiki/page-feed/feed/PageFeed'
import { PageFeedInbox }        from 'domain/wiki/page-feed/inbox/PageFeedInbox'
import { PageFeedSubscription } from 'domain/wiki/page-feed/subscription/PageFeedSubscription'
import { PageIndex }            from 'domain/wiki/pageIndex/PageIndex'
import { Wiki }                 from 'domain/wiki/Wiki'
import {
    ldp ,
    solid
}                               from 'rdf-namespaces'
import { wiki }                 from '../../namespace/Wiki'
import { getTagger }            from '../../utils/logger'
import { spqrqlUpdate }         from '../../utils/Util'

export type WikiCreateArgs =
    {
        pageIndex : PageIndex
        title : string
        uri : string
        config : Config
        inbox : PageFeedInbox
        feed : PageFeed,
        subsciptions : PageFeedSubscription
    }

export class WikiRepository
    implements ICreate<Wiki , WikiCreateArgs> ,
        Get<Wiki> {
    protected tag = getTagger ( WikiRepository.name ).tag

    constructor () {
    }


    async create ( newWIki : WikiCreateArgs ) {
        console.debug ( this.tag ( `Create a new wiki.` ) )
        await spqrqlUpdate (
            newWIki.uri ,
            `INSERT DATA {
                        <> a 
                                            <${ Wiki.type }> , 
                                            <${ ldp.Resource }> 
                        ;
                       
                        <${ wiki.title }> """${ newWIki.title }"""
                        ;
                      
                        <${ wiki.config }> <${ newWIki.config.getPath () }>
                        ;
                        
                        <${ wiki.pageIndex }> <${ newWIki.pageIndex.getPath () }> 
                        ;
                        
                        <${ solid.inbox }> <${ newWIki.inbox.getPath () }> 
                        ;

                        <${ wiki.feed }> <${ newWIki.feed.getPath () }> 
                        ;
                        
                        <${ wiki.subscription }> <${ newWIki.subsciptions.getPath () }> 
                        ;
                        .
                    }`
        )
        return new Wiki ( await data[ newWIki.uri ] )
    }

    async get ( uri : string ) : Promise<Wiki> {
        console.debug ( this.tag ( `Get wiki ${ uri }` ) )
        return new Wiki ( await data[ uri ] )
    }

}

import { Literal } from 'domain/rdf/Literal'
import { Path }    from 'ldflex'
import { wiki }    from '../../../../namespace/Wiki'

export class Title extends Literal<string> {
    static type : string = wiki.Wiki

    constructor (
        private title : Path ,
        value : string
    ) {
        super ( value )
    }

    async setTitle ( title : String ) {
        await this.title.set ( title )
    }

}

import { Node }  from 'domain/rdf/Node'
import { Body }  from 'domain/wiki/page/body/Body'
import { Title } from 'domain/wiki/page/title/Title'
import { Path }  from 'ldflex'
import { wiki }  from '../../../namespace/Wiki'

export class Page extends Node {
    static type : string = wiki.Page

    constructor ( page : Path ) {
        super ( page )
    }

    async getBody () : Promise<Body> {
        return new Body (
            await this.path[ wiki.body ] ,
            await this.path[ wiki.body ].value
        )
    }

    async getTitle () : Promise<Title> {
        return new Title (
            await this.path[ wiki.title ] ,
            await this.path[ wiki.title ].value
        )
    }

}

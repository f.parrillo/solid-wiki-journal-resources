import { TypedIndexRepository } from 'domain/solid/terms/typed-index/TypedIndexRepository'
import { Page }                 from 'domain/wiki/page/Page'
import { PageRepository }       from 'domain/wiki/page/PageRepository'
import { ICreate }              from '../../interfaces/ICreate'

type CreateArgs = { container : string, title : string, body : string }

const formatTitle = ( title : string ) => {
    return title.split ( ' ' )
                .map ( w => w[ 0 ].toUpperCase () + w.substr ( 1 ) )
                .join ( '' )
}

export class PageService implements ICreate<Page , CreateArgs> {
    constructor (
        private  pageRepository      = new PageRepository () ,
        private typedIndexRepository = new TypedIndexRepository ()
    ) {
    }

    async create ( page : { container : string; title : string; body : string } ) : Promise<Page> {
        const uri = `${ page.container }/${ formatTitle ( page.title ) }/page.ttl`
        return await this.pageRepository.create ( { uri : uri , body : page.body , title : page.title } )
    }


    async get ( uri : string ) : Promise<Page> {
        return await this.pageRepository.get ( uri )
    }

    async delete ( uri : string ) {
        return await this.pageRepository.delete ( uri )
    }
}

import data     from '@solid/query-ldflex'
import { Page } from 'domain/wiki/page/Page'
import { ldp }  from 'rdf-namespaces'
import { wiki } from '../../../namespace/Wiki'
import {
    httpDelete ,
    spqrqlUpdate
}               from '../../../utils/Util'
import {
    Get ,
    ICreate
}               from '../../interfaces/ICreate'


type CreateArgs = { uri : string, title : string, body : string }

export class PageRepository
    implements ICreate<Page , CreateArgs> ,
        Get<Page> {

    async create ( wikiPage : CreateArgs ) : Promise<Page> {
        console.log ( `Create page ${ wikiPage.title } in ${ wikiPage.uri }` )

        await spqrqlUpdate (
            wikiPage.uri ,
            `INSERT DATA {
                        <> a 
                                            <${ Page.type }> , 
                                            <${ ldp.Resource }> 
                        ;
                       
                        <${ wiki.title }> """${ wikiPage.title }"""
                        ;
                      
                        <${ wiki.body }> """${ wikiPage.body }""".
                    }`
        )
        return new Page ( await data[ wikiPage.uri ] )
    }

    async get ( uri : string ) {
        return new Page ( await data[ uri ] )
    }

    // todo this might be to aggressive
    async delete ( uri : string ) {
        await httpDelete ( uri )
    }
}

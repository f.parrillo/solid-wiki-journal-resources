import { Literal } from 'domain/rdf/Literal'
import { Path }    from 'ldflex'
import { wiki }    from '../../../../namespace/Wiki'

export class Body extends Literal<string> {
    static type : string = wiki.Page

    constructor (
        private body : Path ,
        value : string
    ) {
        super ( value )
    }

    async setBody ( body : String ) {
        await this.body.set ( body )
    }

}


import { Node }       from 'domain/rdf/Node'
import { SourceList } from 'domain/wiki/Index'
import { Path }       from 'ldflex'
import { wiki }       from '../../../namespace/Wiki'

export class Config extends Node {
    static type : string = wiki.Config

    constructor ( config : Path ) {
        super ( config )
    }

    async getSourceList () : Promise<SourceList> {
        return new SourceList ( await this.path[ wiki.sourceList ] )
    }
}

import { SourceListRepository } from 'domain/wiki/config/source-list/SourceListRepository'
import { SourceList }           from 'domain/wiki/Index'
import { Wiki }                 from 'domain/wiki/Wiki'

export class SourceListService {

    constructor (
        private sourceListRepository = new SourceListRepository ()
    ) {
    }

    async createBlank ( uri : string ) : Promise<SourceList> {
        return this.sourceListRepository.createBlank ( uri )
    }

    async addSourceByWiki (
        wiki : Wiki ,
        source : Wiki
    ) {
        const config     = await wiki.getConfig ()
        const sourceList = await config.getSourceList ()
        await sourceList.add ( source )
    }
}

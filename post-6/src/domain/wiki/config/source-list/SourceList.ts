import { Node } from 'domain/rdf/Node'
import { Wiki } from 'domain/wiki/Wiki'
import { Path } from 'ldflex'
import { wiki } from '../../../../namespace/Wiki'

export class SourceList extends Node {
    static type : string = wiki.SourceList

    constructor ( sourceList : Path ) {
        super ( sourceList )
    }

    async getWikis () : Promise<Wiki[]> {
        return await this.path[ wiki.source ].toArray ()
                                             .then ( proxies => proxies.map ( p => new Wiki ( p ) ) )
    }

    async add ( source : Wiki ) {
        await this.path[ wiki.source ].add ( source.getPath () )
    }
}

import data             from '@solid/query-ldflex'
import { SourceList }   from 'domain/wiki/Index'
import { ldp }          from 'rdf-namespaces'
import { spqrqlUpdate } from '../../../../utils/Util'

export class SourceListRepository {
    constructor () {
    }


    async createBlank ( uri : string ) : Promise<SourceList> {
        await spqrqlUpdate (
            uri ,
            `INSERT DATA {
                        <> a 
                                            <${ SourceList.type }> , 
                                            <${ ldp.Resource }> 
                        ;
                        .
                    }`
        )
        return new SourceList ( await data[ uri ] )
    }
}

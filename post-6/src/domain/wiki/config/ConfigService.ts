import { ConfigRepository } from 'domain/wiki/config/ConfigRepository'
import {
    Config ,
    SourceList
}                           from 'domain/wiki/Index'

export class ConfigService {

    constructor (
        private configRepository = new ConfigRepository ()
    ) {
    }

    async createBlank ( config : { uri : string, sourceList : SourceList } ) : Promise<Config> {
        return this.configRepository.createBlank ( config )
    }
}

import data             from '@solid/query-ldflex'
import {
    Config ,
    SourceList
}                       from 'domain/wiki/Index'
import { ldp }          from 'rdf-namespaces'
import { wiki }         from '../../../namespace/Wiki'
import { spqrqlUpdate } from '../../../utils/Util'

export class ConfigRepository {

    constructor () {
    }

    async createBlank ( config : { uri : string; sourceList : SourceList } ) : Promise<Config> {
        await spqrqlUpdate (
            config.uri ,
            `INSERT DATA {
                        <> a 
                                            <${ Config.type }> , 
                                            <${ ldp.Resource }> 
                        ;
                       
                        <${ wiki.sourceList }> <${ config.sourceList.getPath () }>
                        ;
                        .
                    }`
        )
        return new Config ( await data[ config.uri ] )
    }
}

export interface Get<T> {
    get ( uri : string ) : Promise<T>
}

export interface ICreate<Type , Arguments> {

    create ( args : Arguments ) : Promise<Type>


    // exists(t: string): Promise<T>
    // delete(t: T): Promise<T>
    // getBy(uri: string): Promise<T>
    //
}


import { TurtleResource } from 'domain/iana/media-types/text/turtle/TurtleResource'
import { Resource }       from 'domain/linked-data-platform/Resource'
import { Node }           from 'domain/rdf/Node'
import {
    Path ,
    ResolvedPath
}                         from 'ldflex'
import { ldp }            from 'rdf-namespaces'
import { getTagger }      from '../../utils/logger'


export class Container extends Node {
    static type : string = ldp.Container
    protected tag        = getTagger ( Container.name ).tag

    constructor ( container : Path ) {
        super ( container )
    }

    async getResources () : Promise<( TurtleResource | Container | Resource )[]> {
        console.log ( this.tag ( ( 'Get resources.' ) ) )
        const all : Resource[] =
                  await this.path[ ldp.contains ]
                      .toArray ()
                      .then ( ( resources : ResolvedPath[] ) =>
                                  resources.map ( r => new Resource ( r ) ) )

        const resources : ( TurtleResource | Container | Resource )[]
                  = []

        for ( let r of all ) {
            if ( await r.isOfTypeTurtleResource () ) {
                resources.push ( new TurtleResource ( r.getPath () ) )
            }
            else if ( await r.isOfTypeContainer () ) {
                resources.push ( new Container ( r.getPath () ) )
            }
            else {
                resources.push ( r )
            }
        }

        return resources
    }

    async getTurtleResources () : Promise<TurtleResource[]> {
        console.debug ( this.tag ( 'Get turtle resources' ) )

        const resources = await this.getResources ()
        console.debug ( this.tag ( `Found ${ resources.length } resources.` ) )

        const turtleResources = resources.filter ( r => r instanceof TurtleResource ) as TurtleResource[]
        console.debug ( this.tag ( `Found ${ turtleResources.length } turtle resources.` ) )
        return turtleResources
    }
}

import { TurtleResource } from 'domain/iana/media-types/text/turtle/TurtleResource'
import { Container }      from 'domain/linked-data-platform/Container'
import { Node }           from 'domain/rdf/Node'
import { Path }           from 'ldflex'
import {
    ldp ,
    rdf
}                         from 'rdf-namespaces'
import { getTagger }      from '../../utils/logger'

export class Resource extends Node {
    static type : string = ldp.Resource
    protected tag        = getTagger ( Resource.name ).tag

    constructor ( resource : Path ) {
        super ( resource )
    }

    async isOfTypeTurtleResource () : Promise<boolean> {
        console.debug ( this.tag ( `Is ${ this.getPath () } of type ${ TurtleResource.type }` ) )
        for await ( let type of this.path[ rdf.type ] ) {
            if ( type.toString () === TurtleResource.type ) {
                return true
            }
        }
        return false
    }

    async isOfTypeContainer () : Promise<boolean> {
        console.debug ( this.tag ( `Is ${ this.getPath () } of type ${ Container.type }` ) )
        for await ( let type of this.path[ rdf.type ] ) {
            if ( type.toString () === Container.type ) {
                return true
            }
        }
        return false
    }
}

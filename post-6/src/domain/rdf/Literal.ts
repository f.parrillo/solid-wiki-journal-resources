export abstract class Literal<T extends { toString () : string }> {
    static datatype : string = 'http://parrillo.eu/ns/solid/wiki#Page'

    protected constructor ( protected value : T ) {

    }


    toString () : string {
        return this.value.toString ()
    }
}

@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:Enumeration
    a               rdfs:Class ;
    rdfs:label      "Enumeration" ;
    rdfs:comment    "Lists or enumerations—for example, a list of cuisines or music genres, etc." ;
    rdfs:subClassOf schema:Intangible .

schema:Intangible
    a               rdfs:Class ;
    rdfs:label      "Intangible" ;
    rdfs:comment
                    "A utility class that serves as the umbrella for a number of 'intangible' things such as quantities, structured values, etc." ;
    rdfs:subClassOf schema:Thing .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:ActionStatusType
    rdfs:subClassOf schema:Enumeration .

schema:BoardingPolicyType
    rdfs:subClassOf schema:Enumeration .

schema:BookFormatType
    rdfs:subClassOf schema:Enumeration .

schema:BusinessEntityType
    rdfs:subClassOf schema:Enumeration .

schema:BusinessFunction
    rdfs:subClassOf schema:Enumeration .

schema:ContactPointOption
    rdfs:subClassOf schema:Enumeration .

schema:DayOfWeek
    rdfs:subClassOf schema:Enumeration .

schema:DeliveryMethod
    rdfs:subClassOf schema:Enumeration .

schema:DigitalDocumentPermissionType
    rdfs:subClassOf schema:Enumeration .

schema:EventStatusType
    rdfs:subClassOf schema:Enumeration .

schema:GamePlayMode
    rdfs:subClassOf schema:Enumeration .

schema:GameServerStatus
    rdfs:subClassOf schema:Enumeration .

schema:GenderType
    rdfs:subClassOf schema:Enumeration .

schema:HealthAspectEnumeration
    rdfs:subClassOf schema:Enumeration .

schema:ItemAvailability
    rdfs:subClassOf schema:Enumeration .

schema:ItemListOrderType
    rdfs:subClassOf schema:Enumeration .

schema:LegalForceStatus
    rdfs:subClassOf schema:Enumeration .

schema:LegalValueLevel
    rdfs:subClassOf schema:Enumeration .

schema:MapCategoryType
    rdfs:subClassOf schema:Enumeration .

schema:MedicalEnumeration
    rdfs:subClassOf schema:Enumeration .

schema:MedicalTrialDesign
    rdfs:subClassOf schema:Enumeration .

schema:MerchantReturnEnumeration
    rdfs:subClassOf schema:Enumeration .

schema:MusicAlbumProductionType
    rdfs:subClassOf schema:Enumeration .

schema:MusicAlbumReleaseType
    rdfs:subClassOf schema:Enumeration .

schema:MusicReleaseFormatType
    rdfs:subClassOf schema:Enumeration .

schema:OfferItemCondition
    rdfs:subClassOf schema:Enumeration .

schema:OrderStatus
    rdfs:subClassOf schema:Enumeration .

schema:PaymentMethod
    rdfs:subClassOf schema:Enumeration .

schema:PaymentStatusType
    rdfs:subClassOf schema:Enumeration .

schema:PhysicalActivityCategory
    rdfs:subClassOf schema:Enumeration .

schema:ProductReturnEnumeration
    rdfs:subClassOf schema:Enumeration .

schema:QualitativeValue
    rdfs:subClassOf schema:Enumeration .

schema:RefundTypeEnumeration
    rdfs:subClassOf schema:Enumeration .

schema:ReservationStatusType
    rdfs:subClassOf schema:Enumeration .

schema:RestrictedDiet
    rdfs:subClassOf schema:Enumeration .

schema:ReturnFeesEnumeration
    rdfs:subClassOf schema:Enumeration .

schema:RsvpResponseType
    rdfs:subClassOf schema:Enumeration .

schema:Specialty
    rdfs:subClassOf schema:Enumeration .

schema:WarrantyScope
    rdfs:subClassOf schema:Enumeration .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:supersededBy
    a                     rdf:Property ;
    rdfs:label            "supersededBy" ;
    schema:domainIncludes schema:Class, schema:Enumeration, schema:Property ;
    schema:isPartOf       <http://meta.schema.org> ;
    schema:rangeIncludes  schema:Class, schema:Enumeration, schema:Property ;
    rdfs:comment          "Relates a term (i.e. a property, class or enumeration) to one that supersedes it." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:valueReference
    schema:rangeIncludes schema:Enumeration .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .


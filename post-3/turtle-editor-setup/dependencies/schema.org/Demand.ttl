@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:Demand
    a               rdfs:Class ;
    rdfs:label      "Demand" ;
    dcterms:source  <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsClass> ;
    rdfs:comment
                    "A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply." ;
    rdfs:subClassOf schema:Intangible .

schema:Intangible
    a               rdfs:Class ;
    rdfs:label      "Intangible" ;
    rdfs:comment
                    "A utility class that serves as the umbrella for a number of 'intangible' things such as quantities, structured values, etc." ;
    rdfs:subClassOf schema:Thing .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:acceptedPaymentMethod
    a                     rdf:Property ;
    rdfs:label            "acceptedPaymentMethod" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:LoanOrCredit, schema:PaymentMethod ;
    rdfs:comment          "The payment method(s) accepted by seller for this offer." .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:advanceBookingRequirement
    a                     rdf:Property ;
    rdfs:label            "advanceBookingRequirement" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment
                          "The amount of time that is required between accepting the offer and the actual usage of the resource or service." .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:availability
    a                     rdf:Property ;
    rdfs:label            "availability" ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:ItemAvailability ;
    rdfs:comment          "The availability of this item&#x2014;for example In stock, Out of stock, Pre-order, etc." .

schema:availabilityEnds
    a                     rdf:Property ;
    rdfs:label            "availabilityEnds" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms>,
                          <https://github.com/schemaorg/schemaorg/issues/1741> ;
    schema:category       "issue-1741" ;
    schema:domainIncludes schema:ActionAccessSpecification, schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:Date, schema:DateTime, schema:Time ;
    rdfs:comment          "The end of the availability of the product or service included in the offer." .

schema:availabilityStarts
    a                     rdf:Property ;
    rdfs:label            "availabilityStarts" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms>,
                          <https://github.com/schemaorg/schemaorg/issues/1741> ;
    schema:category       "issue-1741" ;
    schema:domainIncludes schema:ActionAccessSpecification, schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:Date, schema:DateTime, schema:Time ;
    rdfs:comment          "The beginning of the availability of the product or service included in the offer." .

schema:availableAtOrFrom
    a                     rdf:Property ;
    rdfs:label            "availableAtOrFrom" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:Place ;
    rdfs:comment          "The place(s) from which the offer can be obtained (e.g. store locations)." ;
    rdfs:subPropertyOf    schema:areaServed .

schema:availableDeliveryMethod
    a                     rdf:Property ;
    rdfs:label            "availableDeliveryMethod" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:DeliveryMethod ;
    rdfs:comment          "The delivery method(s) available for this offer." .

schema:businessFunction
    a                     rdf:Property ;
    rdfs:label            "businessFunction" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:TypeAndQuantityNode ;
    schema:rangeIncludes  schema:BusinessFunction ;
    rdfs:comment
                          "The business function (e.g. sell, lease, repair, dispose) of the offer or component of a bundle (TypeAndQuantityNode). The default is http://purl.org/goodrelations/v1#Sell." .

schema:deliveryLeadTime
    a                     rdf:Property ;
    rdfs:label            "deliveryLeadTime" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment
                          "The typical delay between the receipt of the order and the goods either leaving the warehouse or being prepared for pickup, in case the delivery method is on site pickup." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:eligibleCustomerType
    a                     rdf:Property ;
    rdfs:label            "eligibleCustomerType" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:BusinessEntityType ;
    rdfs:comment          "The type(s) of customers for which the given offer is valid." .

schema:eligibleDuration
    a                     rdf:Property ;
    rdfs:label            "eligibleDuration" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment          "The duration for which the given offer is valid." .

schema:eligibleQuantity
    a                     rdf:Property ;
    rdfs:label            "eligibleQuantity" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:PriceSpecification ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment
                          "The interval and unit of measurement of ordering quantities for which the offer or price specification is valid. This allows e.g. specifying that a certain freight charge is valid only for a certain quantity." .

schema:eligibleRegion
    a                     rdf:Property ;
    rdfs:label            "eligibleRegion" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms>,
                          <https://github.com/schemaorg/schemaorg/issues/1741> ;
    schema:category       "issue-1741" ;
    schema:domainIncludes schema:ActionAccessSpecification, schema:DeliveryChargeSpecification, schema:Demand,
                          schema:Offer ;
    schema:rangeIncludes  schema:GeoShape, schema:Place, schema:Text ;
    rdfs:comment          """The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is valid.<br/><br/>

See also <a class="localLink" href="http://schema.org/ineligibleRegion">ineligibleRegion</a>.""" ;
    rdfs:subPropertyOf    schema:areaServed .

schema:eligibleTransactionVolume
    a                     rdf:Property ;
    rdfs:label            "eligibleTransactionVolume" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:PriceSpecification ;
    schema:rangeIncludes  schema:PriceSpecification ;
    rdfs:comment
                          "The transaction volume, in a monetary unit, for which the offer or price specification is valid, e.g. for indicating a minimal purchasing volume, to express free shipping above a certain order volume, or to limit the acceptance of credit cards to purchases to a certain minimal amount." .

schema:gtin12
    a                     rdf:Property ;
    rdfs:label            "gtin12" ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The GTIN-12 code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:gtin13
    a                     rdf:Property ;
    rdfs:label            "gtin13" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The GTIN-13 code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:gtin14
    a                     rdf:Property ;
    rdfs:label            "gtin14" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The GTIN-14 code of the product, or the product to which the offer refers. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:gtin8
    a                     rdf:Property ;
    rdfs:label            "gtin8" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The <a href=\"http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx\">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:includesObject
    a                     rdf:Property ;
    rdfs:label            "includesObject" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:TypeAndQuantityNode ;
    rdfs:comment
                          "This links to a node or nodes indicating the exact quantity of the products included in the offer." .

schema:ineligibleRegion
    a                     rdf:Property ;
    rdfs:label            "ineligibleRegion" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2242> ;
    schema:category       "issue-2242" ;
    schema:domainIncludes schema:ActionAccessSpecification, schema:DeliveryChargeSpecification, schema:Demand,
                          schema:Offer ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:GeoShape, schema:Place, schema:Text ;
    rdfs:comment          """The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is not valid, e.g. a region where the transaction is not allowed.<br/><br/>

See also <a class="localLink" href="http://schema.org/eligibleRegion">eligibleRegion</a>.""" .

schema:inventoryLevel
    a                     rdf:Property ;
    rdfs:label            "inventoryLevel" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:SomeProducts ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment          "The current approximate inventory level for the item or items." .

schema:itemCondition
    a                     rdf:Property ;
    rdfs:label            "itemCondition" ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:OfferItemCondition ;
    rdfs:comment
                          "A predefined value from OfferItemCondition or a textual description of the condition of the product or service, or the products or services included in the offer." .

schema:itemOffered
    a                     rdf:Property ;
    rdfs:label            "itemOffered" ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:inverseOf      schema:offers ;
    schema:rangeIncludes  schema:AggregateOffer, schema:CreativeWork, schema:Event, schema:MenuItem, schema:Product,
                          schema:Service, schema:Trip ;
    rdfs:comment
                          "An item being offered (or demanded). The transactional nature of the offer or demand is documented using <a class=\"localLink\" href=\"http://schema.org/businessFunction\">businessFunction</a>, e.g. sell, lease etc. While several common expected types are listed explicitly in this definition, others can be used. Using a second type, such as Product or a subtype of Product, can clarify the nature of the offer." .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:mpn
    a                     rdf:Property ;
    rdfs:label            "mpn" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:priceSpecification
    a                     rdf:Property ;
    rdfs:label            "priceSpecification" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:TradeAction ;
    schema:rangeIncludes  schema:PriceSpecification ;
    rdfs:comment
                          "One or more detailed price specifications, indicating the unit price and delivery or payment charges." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:seeks
    schema:rangeIncludes schema:Demand .

schema:seller
    a                     rdf:Property ;
    rdfs:label            "seller" ;
    schema:domainIncludes schema:BuyAction, schema:Demand, schema:Flight, schema:Offer, schema:Order ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "An entity which offers (sells / leases / lends / loans) the services / goods.  A seller may also be a provider." ;
    rdfs:subPropertyOf    schema:participant .

schema:serialNumber
    a                     rdf:Property ;
    rdfs:label            "serialNumber" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:IndividualProduct, schema:Offer ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The serial number or any alphanumeric identifier of a particular product. When attached to an offer, it is a shortcut for the serial number of the product included in the offer." ;
    rdfs:subPropertyOf    schema:identifier .

schema:sku
    a                     rdf:Property ;
    rdfs:label            "sku" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers." ;
    rdfs:subPropertyOf    schema:identifier .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:validFrom
    a                     rdf:Property ;
    rdfs:label            "validFrom" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:LocationFeatureSpecification, schema:MonetaryAmount, schema:Offer,
                          schema:OpeningHoursSpecification, schema:Permit, schema:PriceSpecification ;
    schema:rangeIncludes  schema:Date, schema:DateTime ;
    rdfs:comment          "The date when the item becomes valid." .

schema:validThrough
    a                     rdf:Property ;
    rdfs:label            "validThrough" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:JobPosting, schema:LocationFeatureSpecification, schema:MonetaryAmount,
                          schema:Offer, schema:OpeningHoursSpecification, schema:PriceSpecification ;
    schema:rangeIncludes  schema:Date, schema:DateTime ;
    rdfs:comment
                          "The date after when the item is not valid. For example the end of an offer, salary period, or a period of opening hours." .

schema:warranty
    a                     rdf:Property ;
    rdfs:label            "warranty" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer ;
    schema:rangeIncludes  schema:WarrantyPromise ;
    rdfs:comment          "The warranty promise(s) included in the offer." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

schema:offers
    schema:rangeIncludes schema:Demand .

schema:areaServed
    a                     rdf:Property ;
    rdfs:label            "areaServed" ;
    schema:domainIncludes schema:ContactPoint, schema:DeliveryChargeSpecification, schema:Demand, schema:Offer,
                          schema:Organization, schema:Service ;
    schema:rangeIncludes  schema:AdministrativeArea, schema:GeoShape, schema:Place, schema:Text ;
    rdfs:comment          "The geographic area where a service or offered item is provided." .

schema:gtin
    a                     rdf:Property ;
    rdfs:label            "gtin" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2288> ;
    schema:category       "issue-1244" ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A Global Trade Item Number (<a href=\"https://www.gs1.org/standards/id-keys/gtin\">GTIN</a>). GTINs identify trade items, including products and services, using numeric identification codes. The <a class=\"localLink\" href=\"http://schema.org/gtin\">gtin</a> property generalizes the earlier <a class=\"localLink\" href=\"http://schema.org/gtin8\">gtin8</a>, <a class=\"localLink\" href=\"http://schema.org/gtin12\">gtin12</a>, <a class=\"localLink\" href=\"http://schema.org/gtin13\">gtin13</a>, and <a class=\"localLink\" href=\"http://schema.org/gtin14\">gtin14</a> properties. The GS1 <a href=\"https://www.gs1.org/standards/Digital-Link/\">digital link specifications</a> express GTINs as URLs. A correct <a class=\"localLink\" href=\"http://schema.org/gtin\">gtin</a> value should be a valid GTIN, which means that it should be an all-numeric string of either 8, 12, 13 or 14 digits, or a \"GS1 Digital Link\" URL based on such a string. The numeric component should also have a <a href=\"https://www.gs1.org/services/check-digit-calculator\">valid GS1 check digit</a> and meet the other rules for valid GTINs. See also <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1's GTIN Summary</a> and <a href=\"https://en.wikipedia.org/wiki/Global_Trade_Item_Number\">Wikipedia</a> for more details. Left-padding of the gtin values is not required or encouraged." ;
    rdfs:subPropertyOf    schema:identifier .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .


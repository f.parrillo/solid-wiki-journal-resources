@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:Intangible
    a               rdfs:Class ;
    rdfs:label      "Intangible" ;
    rdfs:comment
                    "A utility class that serves as the umbrella for a number of 'intangible' things such as quantities, structured values, etc." ;
    rdfs:subClassOf schema:Thing .

schema:LinkRole
    a               rdfs:Class ;
    rdfs:label      "LinkRole" ;
    dcterms:source  <https://github.com/schemaorg/schemaorg/issues/1045> ;
    schema:category "issue-1045" ;
    schema:isPartOf <http://pending.schema.org> ;
    rdfs:comment
                    "A Role that represents a Web link e.g. as expressed via the 'url' property. Its linkRelationship property can indicate URL-based and plain textual link types e.g. those in IANA link registry or others such as 'amphtml'. This structure provides a placeholder where details from HTML's link element can be represented outside of HTML, e.g. in JSON-LD feeds." ;
    rdfs:subClassOf schema:Role .

schema:Role
    a               rdfs:Class ;
    rdfs:label      "Role" ;
    rdfs:comment    """Represents additional information about a relationship or property. For example a Role can be used to say that a 'member' role linking some SportsTeam to a player occurred during a particular time period. Or that a Person's 'actor' role in a Movie was for some particular characterName. Such properties can be attached to a Role entity, which is then associated with the main entities using ordinary properties like 'member' or 'actor'.<br/><br/>

See also <a href="http://blog.schema.org/2014/06/introducing-role.html">blog post</a>.""" ;
    rdfs:subClassOf schema:Intangible .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:endDate
    a                     rdf:Property ;
    rdfs:label            "endDate" ;
    schema:domainIncludes schema:CreativeWorkSeason, schema:CreativeWorkSeries, schema:DatedMoneySpecification,
                          schema:EducationalOccupationalProgram, schema:Event, schema:Role ;
    schema:rangeIncludes  schema:Date, schema:DateTime ;
    rdfs:comment
                          "The end date and time of the item (in <a href=\"http://en.wikipedia.org/wiki/ISO_8601\">ISO 8601 date format</a>)." .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:inLanguage
    a                     rdf:Property ;
    rdfs:label            "inLanguage" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2382> ;
    schema:category       "issue-2382" ;
    schema:domainIncludes schema:BroadcastService, schema:CommunicateAction, schema:CreativeWork, schema:Event,
                          schema:LinkRole, schema:PronounceableText, schema:WriteAction ;
    schema:rangeIncludes  schema:Language, schema:Text ;
    rdfs:comment
                          "The language of the content or performance or used in an action. Please use one of the language codes from the <a href=\"http://tools.ietf.org/html/bcp47\">IETF BCP 47 standard</a>. See also <a class=\"localLink\" href=\"http://schema.org/availableLanguage\">availableLanguage</a>." .

schema:linkRelationship
    a                     rdf:Property ;
    rdfs:label            "linkRelationship" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1045> ;
    schema:category       "issue-1045" ;
    schema:domainIncludes schema:LinkRole ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "Indicates the relationship type of a Web link." .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:namedPosition
    a                     rdf:Property ;
    rdfs:label            "namedPosition" ;
    schema:domainIncludes schema:Role ;
    schema:rangeIncludes  schema:Text, schema:URL ;
    schema:supersededBy   schema:roleName ;
    rdfs:comment
                          "A position played, performed or filled by a person or organization, as part of an organization. For example, an athlete in a SportsTeam might play in the position named 'Quarterback'." .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:startDate
    a                     rdf:Property ;
    rdfs:label            "startDate" ;
    schema:domainIncludes schema:CreativeWorkSeason, schema:CreativeWorkSeries, schema:DatedMoneySpecification,
                          schema:EducationalOccupationalProgram, schema:Event, schema:Role ;
    schema:rangeIncludes  schema:Date, schema:DateTime ;
    rdfs:comment
                          "The start date and time of the item (in <a href=\"http://en.wikipedia.org/wiki/ISO_8601\">ISO 8601 date format</a>)." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

schema:roleName
    a                     rdf:Property ;
    rdfs:label            "roleName" ;
    schema:domainIncludes schema:Role ;
    schema:rangeIncludes  schema:Text, schema:URL ;
    rdfs:comment
                          "A role played, performed or filled by a person or organization. For example, the team of creators for a comic book might fill the roles named 'inker', 'penciller', and 'letterer'; or an athlete in a SportsTeam might play in the position named 'Quarterback'." .


@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:Product
    a               rdfs:Class ;
    rdfs:label      "Product" ;
    dcterms:source  <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    rdfs:comment
                    "Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online." ;
    rdfs:subClassOf schema:Thing .

schema:SomeProducts
    a               rdfs:Class ;
    rdfs:label      "SomeProducts" ;
    dcterms:source  <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsClass> ;
    rdfs:comment    "A placeholder for multiple similar products of the same kind." ;
    rdfs:subClassOf schema:Product .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:additionalProperty
    a                     rdf:Property ;
    rdfs:label            "additionalProperty" ;
    schema:domainIncludes schema:Place, schema:Product, schema:QualitativeValue, schema:QuantitativeValue ;
    schema:rangeIncludes  schema:PropertyValue ;
    rdfs:comment          """A property-value pair representing an additional characteristics of the entitity, e.g. a product feature or another characteristic for which there is no matching property in schema.org.<br/><br/>

Note: Publishers should be aware that applications designed to use specific schema.org properties (e.g. http://schema.org/width, http://schema.org/color, http://schema.org/gtin13, ...) will typically expect such data to be provided using those properties, rather than using the generic property/value mechanism.""" .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:aggregateRating
    a                     rdf:Property ;
    rdfs:label            "aggregateRating" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:AggregateRating ;
    rdfs:comment          "The overall rating, based on a collection of reviews or ratings, of the item." .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:audience
    a                     rdf:Property ;
    rdfs:label            "audience" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:LodgingBusiness, schema:PlayAction, schema:Product,
                          schema:Service ;
    schema:rangeIncludes  schema:Audience ;
    rdfs:comment          "An intended audience, i.e. a group for whom something was created." .

schema:awards
    a                     rdf:Property ;
    rdfs:label            "awards" ;
    schema:domainIncludes schema:CreativeWork, schema:Organization, schema:Person, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    schema:supersededBy   schema:award ;
    rdfs:comment          "Awards won by or for this item." .

schema:brand
    a                     rdf:Property ;
    rdfs:label            "brand" ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Brand, schema:Organization ;
    rdfs:comment
                          "The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person." .

schema:category
    a                     rdf:Property ;
    rdfs:label            "category" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1741> ;
    schema:category       "issue-1741" ;
    schema:domainIncludes schema:ActionAccessSpecification, schema:Invoice, schema:Offer, schema:PhysicalActivity,
                          schema:Product, schema:Recommendation, schema:Service ;
    schema:rangeIncludes  schema:PhysicalActivityCategory, schema:Text, schema:Thing ;
    rdfs:comment
                          "A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy." .

schema:color
    a                     rdf:Property ;
    rdfs:label            "color" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The color of the product." .

schema:depth
    a                     rdf:Property ;
    rdfs:label            "depth" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product, schema:VisualArtwork ;
    schema:rangeIncludes  schema:Distance, schema:QuantitativeValue ;
    rdfs:comment          "The depth of the item." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:gtin12
    a                     rdf:Property ;
    rdfs:label            "gtin12" ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The GTIN-12 code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:gtin13
    a                     rdf:Property ;
    rdfs:label            "gtin13" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The GTIN-13 code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:gtin14
    a                     rdf:Property ;
    rdfs:label            "gtin14" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The GTIN-14 code of the product, or the product to which the offer refers. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:gtin8
    a                     rdf:Property ;
    rdfs:label            "gtin8" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The <a href=\"http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx\">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1 GTIN Summary</a> for more details." ;
    rdfs:subPropertyOf    schema:gtin, schema:identifier .

schema:hasProductReturnPolicy
    a                     rdf:Property ;
    rdfs:label            "hasProductReturnPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2288> ;
    schema:category       "issue-2288" ;
    schema:domainIncludes schema:Organization, schema:Product ;
    schema:isPartOf       <http://attic.schema.org> ;
    schema:rangeIncludes  schema:ProductReturnPolicy ;
    schema:supersededBy   schema:hasMerchantReturnPolicy ;
    rdfs:comment          "Indicates a ProductReturnPolicy that may be applicable." .

schema:height
    a                     rdf:Property ;
    rdfs:label            "height" ;
    schema:domainIncludes schema:MediaObject, schema:Person, schema:Product, schema:VisualArtwork ;
    schema:rangeIncludes  schema:Distance, schema:QuantitativeValue ;
    rdfs:comment          "The height of the item." .

schema:inventoryLevel
    a                     rdf:Property ;
    rdfs:label            "inventoryLevel" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:SomeProducts ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment          "The current approximate inventory level for the item or items." .

schema:isAccessoryOrSparePartFor
    a                     rdf:Property ;
    rdfs:label            "isAccessoryOrSparePartFor" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product ;
    schema:rangeIncludes  schema:Product ;
    rdfs:comment
                          "A pointer to another product (or multiple products) for which this product is an accessory or spare part." .

schema:isConsumableFor
    a                     rdf:Property ;
    rdfs:label            "isConsumableFor" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product ;
    schema:rangeIncludes  schema:Product ;
    rdfs:comment
                          "A pointer to another product (or multiple products) for which this product is a consumable." .

schema:isRelatedTo
    a                     rdf:Property ;
    rdfs:label            "isRelatedTo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Product, schema:Service ;
    rdfs:comment          "A pointer to another, somehow related product (or multiple products)." .

schema:isSimilarTo
    a                     rdf:Property ;
    rdfs:label            "isSimilarTo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Product, schema:Service ;
    rdfs:comment          "A pointer to another, functionally similar product (or multiple products)." .

schema:itemCondition
    a                     rdf:Property ;
    rdfs:label            "itemCondition" ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:OfferItemCondition ;
    rdfs:comment
                          "A predefined value from OfferItemCondition or a textual description of the condition of the product or service, or the products or services included in the offer." .

schema:logo
    a                     rdf:Property ;
    rdfs:label            "logo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment          "An associated logo." ;
    rdfs:subPropertyOf    schema:image .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:manufacturer
    a                     rdf:Property ;
    rdfs:label            "manufacturer" ;
    schema:domainIncludes schema:DietarySupplement, schema:Drug, schema:Product ;
    schema:rangeIncludes  schema:Organization ;
    rdfs:comment          "The manufacturer of the product." .

schema:material
    a                     rdf:Property ;
    rdfs:label            "material" ;
    schema:domainIncludes schema:CreativeWork, schema:Product ;
    schema:rangeIncludes  schema:Product, schema:Text, schema:URL ;
    rdfs:comment          "A material that something is made from, e.g. leather, wool, cotton, paper." .

schema:model
    a                     rdf:Property ;
    rdfs:label            "model" ;
    schema:domainIncludes schema:Product ;
    schema:rangeIncludes  schema:ProductModel, schema:Text ;
    rdfs:comment
                          "The model of the product. Use with the URL of a ProductModel or a textual representation of the model identifier. The URL of the ProductModel can be from an external source. It is recommended to additionally provide strong product identifiers via the gtin8/gtin13/gtin14 and mpn properties." .

schema:mpn
    a                     rdf:Property ;
    rdfs:label            "mpn" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:nsn
    a                     rdf:Property ;
    rdfs:label            "nsn" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2126> ;
    schema:category       "issue-2126" ;
    schema:domainIncludes schema:Product ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "Indicates the <a href=\"https://en.wikipedia.org/wiki/NATO_Stock_Number\">NATO stock number</a> (nsn) of a <a class=\"localLink\" href=\"http://schema.org/Product\">Product</a>." ;
    rdfs:subPropertyOf    schema:identifier .

schema:offers
    a                     rdf:Property ;
    rdfs:label            "offers" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2289> ;
    schema:category       "issue-2289" ;
    schema:domainIncludes schema:AggregateOffer, schema:CreativeWork, schema:EducationalOccupationalProgram,
                          schema:Event, schema:MenuItem, schema:Product, schema:Service, schema:Trip ;
    schema:inverseOf      schema:itemOffered ;
    schema:rangeIncludes  schema:Demand, schema:Offer ;
    rdfs:comment
                          "An offer to provide this item&#x2014;for example, an offer to sell a product, rent the DVD of a movie, perform a service, or give away tickets to an event. Use <a class=\"localLink\" href=\"http://schema.org/businessFunction\">businessFunction</a> to indicate the kind of transaction offered, i.e. sell, lease, etc. This property can also be used to describe a <a class=\"localLink\" href=\"http://schema.org/Demand\">Demand</a>. While this property is listed as expected on a number of common types, it can be used in others. In that case, using a second type, such as Product or a subtype of Product, can clarify the nature of the offer." .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:productID
    a                     rdf:Property ;
    rdfs:label            "productID" ;
    schema:domainIncludes schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The product identifier, such as ISBN. For example: <code>meta itemprop=\"productID\" content=\"isbn:123-456-789\"</code>." ;
    rdfs:subPropertyOf    schema:identifier .

schema:productionDate
    a                     rdf:Property ;
    rdfs:label            "productionDate" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#Automotive_Ontology_Working_Group> ;
    schema:domainIncludes schema:Product, schema:Vehicle ;
    schema:rangeIncludes  schema:Date ;
    rdfs:comment          "The date of production of the item, e.g. vehicle." .

schema:purchaseDate
    a                     rdf:Property ;
    rdfs:label            "purchaseDate" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#Automotive_Ontology_Working_Group> ;
    schema:domainIncludes schema:Product, schema:Vehicle ;
    schema:rangeIncludes  schema:Date ;
    rdfs:comment          "The date the item e.g. vehicle was purchased by the current owner." .

schema:releaseDate
    a                     rdf:Property ;
    rdfs:label            "releaseDate" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product ;
    schema:rangeIncludes  schema:Date ;
    rdfs:comment
                          "The release date of a product or product model. This can be used to distinguish the exact variant of a product." .

schema:reviews
    a                     rdf:Property ;
    rdfs:label            "reviews" ;
    schema:domainIncludes schema:CreativeWork, schema:Offer, schema:Organization, schema:Place, schema:Product ;
    schema:rangeIncludes  schema:Review ;
    schema:supersededBy   schema:review ;
    rdfs:comment          "Review of the item." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:sku
    a                     rdf:Property ;
    rdfs:label            "sku" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers." ;
    rdfs:subPropertyOf    schema:identifier .

schema:slogan
    a                     rdf:Property ;
    rdfs:label            "slogan" ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "A slogan or motto associated with the item." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:weight
    a                     rdf:Property ;
    rdfs:label            "weight" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Person, schema:Product ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment          "The weight of the product or person." .

schema:width
    a                     rdf:Property ;
    rdfs:label            "width" ;
    schema:domainIncludes schema:MediaObject, schema:Product, schema:VisualArtwork ;
    schema:rangeIncludes  schema:Distance, schema:QuantitativeValue ;
    rdfs:comment          "The width of the item." .

schema:award
    a                     rdf:Property ;
    rdfs:label            "award" ;
    schema:domainIncludes schema:CreativeWork, schema:Organization, schema:Person, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An award won by or for this item." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

schema:hasMerchantReturnPolicy
    a                     rdf:Property ;
    rdfs:label            "hasMerchantReturnPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2288> ;
    schema:category       "issue-2288" ;
    schema:domainIncludes schema:Organization, schema:Product ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:MerchantReturnPolicy ;
    rdfs:comment          "Indicates a MerchantReturnPolicy that may be applicable." .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:review
    a                     rdf:Property ;
    rdfs:label            "review" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Review ;
    rdfs:comment          "A review of the item." .

schema:gtin
    a                     rdf:Property ;
    rdfs:label            "gtin" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2288> ;
    schema:category       "issue-1244" ;
    schema:domainIncludes schema:Demand, schema:Offer, schema:Product ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A Global Trade Item Number (<a href=\"https://www.gs1.org/standards/id-keys/gtin\">GTIN</a>). GTINs identify trade items, including products and services, using numeric identification codes. The <a class=\"localLink\" href=\"http://schema.org/gtin\">gtin</a> property generalizes the earlier <a class=\"localLink\" href=\"http://schema.org/gtin8\">gtin8</a>, <a class=\"localLink\" href=\"http://schema.org/gtin12\">gtin12</a>, <a class=\"localLink\" href=\"http://schema.org/gtin13\">gtin13</a>, and <a class=\"localLink\" href=\"http://schema.org/gtin14\">gtin14</a> properties. The GS1 <a href=\"https://www.gs1.org/standards/Digital-Link/\">digital link specifications</a> express GTINs as URLs. A correct <a class=\"localLink\" href=\"http://schema.org/gtin\">gtin</a> value should be a valid GTIN, which means that it should be an all-numeric string of either 8, 12, 13 or 14 digits, or a \"GS1 Digital Link\" URL based on such a string. The numeric component should also have a <a href=\"https://www.gs1.org/services/check-digit-calculator\">valid GS1 check digit</a> and meet the other rules for valid GTINs. See also <a href=\"http://www.gs1.org/barcodes/technical/idkeys/gtin\">GS1's GTIN Summary</a> and <a href=\"https://en.wikipedia.org/wiki/Global_Trade_Item_Number\">Wikipedia</a> for more details. Left-padding of the gtin values is not required or encouraged." ;
    rdfs:subPropertyOf    schema:identifier .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .


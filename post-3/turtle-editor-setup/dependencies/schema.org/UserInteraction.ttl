@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:Event
    a                   rdfs:Class ;
    rdfs:label          "Event" ;
    rdfs:comment
                        "An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the <a class=\"localLink\" href=\"http://schema.org/offers\">offers</a> property. Repeated events may be structured as separate Event objects." ;
    rdfs:subClassOf     schema:Thing ;
    owl:equivalentClass dctype:Event .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:UserInteraction
    a                   rdfs:Class ;
    rdfs:label          "UserInteraction" ;
    schema:supersededBy schema:InteractionCounter ;
    rdfs:comment
                        "UserInteraction and its subtypes is an old way of talking about users interacting with pages. It is generally better to use <a class=\"localLink\" href=\"http://schema.org/Action\">Action</a>-based vocabulary, alongside types such as <a class=\"localLink\" href=\"http://schema.org/Comment\">Comment</a>." ;
    rdfs:subClassOf     schema:Event .

schema:UserBlocks
    rdfs:subClassOf schema:UserInteraction .

schema:UserCheckins
    rdfs:subClassOf schema:UserInteraction .

schema:UserComments
    rdfs:subClassOf schema:UserInteraction .

schema:UserDownloads
    rdfs:subClassOf schema:UserInteraction .

schema:UserLikes
    rdfs:subClassOf schema:UserInteraction .

schema:UserPageVisits
    rdfs:subClassOf schema:UserInteraction .

schema:UserPlays
    rdfs:subClassOf schema:UserInteraction .

schema:UserPlusOnes
    rdfs:subClassOf schema:UserInteraction .

schema:UserTweets
    rdfs:subClassOf schema:UserInteraction .

schema:actor
    a                     rdf:Property ;
    rdfs:label            "actor" ;
    schema:domainIncludes schema:Clip, schema:CreativeWorkSeason, schema:Episode, schema:Event, schema:Movie,
                          schema:MovieSeries, schema:RadioSeries, schema:TVSeries, schema:VideoGame,
                          schema:VideoGameSeries, schema:VideoObject ;
    schema:rangeIncludes  schema:Person ;
    rdfs:comment
                          "An actor, e.g. in tv, radio, movie, video games etc., or in an event. Actors can be associated with individual items or with a series, episode, clip." .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:aggregateRating
    a                     rdf:Property ;
    rdfs:label            "aggregateRating" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:AggregateRating ;
    rdfs:comment          "The overall rating, based on a collection of reviews or ratings, of the item." .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:attendees
    a                     rdf:Property ;
    rdfs:label            "attendees" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    schema:supersededBy   schema:attendee ;
    rdfs:comment          "A person attending the event." .

schema:audience
    a                     rdf:Property ;
    rdfs:label            "audience" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:LodgingBusiness, schema:PlayAction, schema:Product,
                          schema:Service ;
    schema:rangeIncludes  schema:Audience ;
    rdfs:comment          "An intended audience, i.e. a group for whom something was created." .

schema:composer
    a                     rdf:Property ;
    rdfs:label            "composer" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#MBZ> ;
    schema:domainIncludes schema:Event, schema:MusicComposition ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "The person or organization who wrote a composition, or who is the composer of a work performed at some event." .

schema:contributor
    a                     rdf:Property ;
    rdfs:label            "contributor" ;
    schema:domainIncludes schema:CreativeWork, schema:Event ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment          "A secondary contributor to the CreativeWork or Event." .

schema:director
    a                     rdf:Property ;
    rdfs:label            "director" ;
    schema:domainIncludes schema:Clip, schema:CreativeWorkSeason, schema:Episode, schema:Event, schema:Movie,
                          schema:MovieSeries, schema:RadioSeries, schema:TVSeries, schema:VideoGame,
                          schema:VideoGameSeries, schema:VideoObject ;
    schema:rangeIncludes  schema:Person ;
    rdfs:comment
                          "A director of e.g. tv, radio, movie, video gaming etc. content, or of an event. Directors can be associated with individual items or with a series, episode, clip." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:doorTime
    a                     rdf:Property ;
    rdfs:label            "doorTime" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:DateTime, schema:Time ;
    rdfs:comment          "The time admission will commence." .

schema:duration
    a                     rdf:Property ;
    rdfs:label            "duration" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1457>,
                          <https://github.com/schemaorg/schemaorg/issues/1698> ;
    schema:category       "issue-1457", "issue-1698" ;
    schema:domainIncludes schema:Audiobook, schema:Event, schema:MediaObject, schema:Movie, schema:MusicRecording,
                          schema:MusicRelease, schema:QuantitativeValueDistribution, schema:Schedule ;
    schema:rangeIncludes  schema:Duration ;
    rdfs:comment
                          "The duration of the item (movie, audio recording, event, etc.) in <a href=\"http://en.wikipedia.org/wiki/ISO_8601\">ISO 8601 date format</a>." .

schema:endDate
    a                     rdf:Property ;
    rdfs:label            "endDate" ;
    schema:domainIncludes schema:CreativeWorkSeason, schema:CreativeWorkSeries, schema:DatedMoneySpecification,
                          schema:EducationalOccupationalProgram, schema:Event, schema:Role ;
    schema:rangeIncludes  schema:Date, schema:DateTime ;
    rdfs:comment
                          "The end date and time of the item (in <a href=\"http://en.wikipedia.org/wiki/ISO_8601\">ISO 8601 date format</a>)." .

schema:eventSchedule
    a                     rdf:Property ;
    rdfs:label            "eventSchedule" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1457> ;
    schema:category       "issue-1457" ;
    schema:domainIncludes schema:Event ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Schedule ;
    rdfs:comment          """Associates an <a class="localLink" href="http://schema.org/Event">Event</a> with a <a class="localLink" href="http://schema.org/Schedule">Schedule</a>. There are circumstances where it is preferable to share a schedule for a series of
      repeating events rather than data on the individual events themselves. For example, a website or application might prefer to publish a schedule for a weekly
      gym class rather than provide data on every event. A schedule could be processed by applications to add forthcoming events to a calendar. An <a class="localLink" href="http://schema.org/Event">Event</a> that
      is associated with a <a class="localLink" href="http://schema.org/Schedule">Schedule</a> using this property should not have <a class="localLink" href="http://schema.org/startDate">startDate</a> or <a class="localLink" href="http://schema.org/endDate">endDate</a> properties. These are instead defined within the associated
      <a class="localLink" href="http://schema.org/Schedule">Schedule</a>, this avoids any ambiguity for clients using the data. The property might have repeated values to specify different schedules, e.g. for different months
      or seasons.""" .

schema:eventStatus
    a                     rdf:Property ;
    rdfs:label            "eventStatus" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:EventStatusType ;
    rdfs:comment
                          "An eventStatus of an event represents its status; particularly useful when an event is cancelled or rescheduled." .

schema:funder
    a                     rdf:Property ;
    rdfs:label            "funder" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:MonetaryGrant, schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "A person or organization that supports (sponsors) something through some kind of financial contribution." ;
    rdfs:subPropertyOf    schema:sponsor .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:inLanguage
    a                     rdf:Property ;
    rdfs:label            "inLanguage" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2382> ;
    schema:category       "issue-2382" ;
    schema:domainIncludes schema:BroadcastService, schema:CommunicateAction, schema:CreativeWork, schema:Event,
                          schema:LinkRole, schema:PronounceableText, schema:WriteAction ;
    schema:rangeIncludes  schema:Language, schema:Text ;
    rdfs:comment
                          "The language of the content or performance or used in an action. Please use one of the language codes from the <a href=\"http://tools.ietf.org/html/bcp47\">IETF BCP 47 standard</a>. See also <a class=\"localLink\" href=\"http://schema.org/availableLanguage\">availableLanguage</a>." .

schema:isAccessibleForFree
    a                     rdf:Property ;
    rdfs:label            "isAccessibleForFree" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:Place, schema:PublicationEvent ;
    schema:rangeIncludes  schema:Boolean ;
    rdfs:comment          "A flag to signal that the item, event, or place is accessible for free." .

schema:location
    a                     rdf:Property ;
    rdfs:label            "location" ;
    schema:domainIncludes schema:Action, schema:Event, schema:Organization ;
    schema:rangeIncludes  schema:Place, schema:PostalAddress, schema:Text ;
    rdfs:comment
                          "The location of for example where the event is happening, an organization is located, or where an action takes place." .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:maximumAttendeeCapacity
    a                     rdf:Property ;
    rdfs:label            "maximumAttendeeCapacity" ;
    schema:domainIncludes schema:Event, schema:Place ;
    schema:rangeIncludes  schema:Integer ;
    rdfs:comment          "The total number of individuals that may attend an event or venue." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:offers
    a                     rdf:Property ;
    rdfs:label            "offers" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2289> ;
    schema:category       "issue-2289" ;
    schema:domainIncludes schema:AggregateOffer, schema:CreativeWork, schema:EducationalOccupationalProgram,
                          schema:Event, schema:MenuItem, schema:Product, schema:Service, schema:Trip ;
    schema:inverseOf      schema:itemOffered ;
    schema:rangeIncludes  schema:Demand, schema:Offer ;
    rdfs:comment
                          "An offer to provide this item&#x2014;for example, an offer to sell a product, rent the DVD of a movie, perform a service, or give away tickets to an event. Use <a class=\"localLink\" href=\"http://schema.org/businessFunction\">businessFunction</a> to indicate the kind of transaction offered, i.e. sell, lease, etc. This property can also be used to describe a <a class=\"localLink\" href=\"http://schema.org/Demand\">Demand</a>. While this property is listed as expected on a number of common types, it can be used in others. In that case, using a second type, such as Product or a subtype of Product, can clarify the nature of the offer." .

schema:organizer
    a                     rdf:Property ;
    rdfs:label            "organizer" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment          "An organizer of an Event." .

schema:performers
    a                     rdf:Property ;
    rdfs:label            "performers" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    schema:supersededBy   schema:performer ;
    rdfs:comment
                          "The main performer or performers of the event&#x2014;for example, a presenter, musician, or actor." .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:previousStartDate
    a                     rdf:Property ;
    rdfs:label            "previousStartDate" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Date ;
    rdfs:comment
                          "Used in conjunction with eventStatus for rescheduled or cancelled events. This property contains the previously scheduled start date. For rescheduled events, the startDate property should be used for the newly scheduled start date. In the (rare) case of an event that has been postponed and rescheduled multiple times, this field may be repeated." .

schema:recordedIn
    a                     rdf:Property ;
    rdfs:label            "recordedIn" ;
    schema:domainIncludes schema:Event ;
    schema:inverseOf      schema:recordedAt ;
    schema:rangeIncludes  schema:CreativeWork ;
    rdfs:comment          "The CreativeWork that captured all or part of this Event." .

schema:remainingAttendeeCapacity
    a                     rdf:Property ;
    rdfs:label            "remainingAttendeeCapacity" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Integer ;
    rdfs:comment          "The number of attendee places for an event that remain unallocated." .

schema:review
    a                     rdf:Property ;
    rdfs:label            "review" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Review ;
    rdfs:comment          "A review of the item." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:startDate
    a                     rdf:Property ;
    rdfs:label            "startDate" ;
    schema:domainIncludes schema:CreativeWorkSeason, schema:CreativeWorkSeries, schema:DatedMoneySpecification,
                          schema:EducationalOccupationalProgram, schema:Event, schema:Role ;
    schema:rangeIncludes  schema:Date, schema:DateTime ;
    rdfs:comment
                          "The start date and time of the item (in <a href=\"http://en.wikipedia.org/wiki/ISO_8601\">ISO 8601 date format</a>)." .

schema:subEvents
    a                     rdf:Property ;
    rdfs:label            "subEvents" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Event ;
    schema:supersededBy   schema:subEvent ;
    rdfs:comment
                          "Events that are a part of this event. For example, a conference event includes many presentations, each subEvents of the conference." .

schema:translator
    a                     rdf:Property ;
    rdfs:label            "translator" ;
    schema:domainIncludes schema:CreativeWork, schema:Event ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "Organization or person who adapts a creative work to different languages, regional differences and technical requirements of a target market, or that translates during some event." .

schema:typicalAgeRange
    a                     rdf:Property ;
    rdfs:label            "typicalAgeRange" ;
    schema:domainIncludes schema:CreativeWork, schema:Event ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The typical expected age range, e.g. '7-9', '11-'." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:workPerformed
    a                     rdf:Property ;
    rdfs:label            "workPerformed" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:CreativeWork ;
    rdfs:comment          "A work performed in some event, for example a play performed in a TheaterEvent." ;
    rdfs:subPropertyOf    schema:workFeatured .

schema:about
    a                     rdf:Property ;
    rdfs:label            "about" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:CommunicateAction, schema:CreativeWork, schema:Event ;
    schema:inverseOf      schema:subjectOf ;
    schema:rangeIncludes  schema:Thing ;
    rdfs:comment          "The subject matter of the content." .

schema:attendee
    a                     rdf:Property ;
    rdfs:label            "attendee" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment          "A person or organization attending the event." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

schema:performer
    a                     rdf:Property ;
    rdfs:label            "performer" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "A performer at the event&#x2014;for example, a presenter, musician, musical group or actor." .

schema:sponsor
    a                     rdf:Property ;
    rdfs:label            "sponsor" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:Grant, schema:MedicalStudy, schema:Organization,
                          schema:Person ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "A person or organization that supports a thing through a pledge, promise, or financial contribution. e.g. a sponsor of a Medical Study or a corporate sponsor of an event." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:superEvent
    a                     rdf:Property ;
    rdfs:label            "superEvent" ;
    schema:domainIncludes schema:Event ;
    schema:inverseOf      schema:subEvent ;
    schema:rangeIncludes  schema:Event ;
    rdfs:comment
                          "An event that this event is a part of. For example, a collection of individual music performances might each have a music festival as their superEvent." .

schema:workFeatured
    a                     rdf:Property ;
    rdfs:label            "workFeatured" ;
    schema:domainIncludes schema:Event ;
    schema:rangeIncludes  schema:CreativeWork ;
    rdfs:comment          """A work featured in some event, e.g. exhibited in an ExhibitionEvent.
       Specific subproperties are available for workPerformed (e.g. a play), or a workPresented (a Movie at a ScreeningEvent).""" .

schema:subEvent
    a                     rdf:Property ;
    rdfs:label            "subEvent" ;
    schema:domainIncludes schema:Event ;
    schema:inverseOf      schema:superEvent ;
    schema:rangeIncludes  schema:Event ;
    rdfs:comment
                          "An Event that is part of this event. For example, a conference event includes many presentations, each of which is a subEvent of the conference." .


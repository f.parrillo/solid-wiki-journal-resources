@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:FoodService
    a               rdfs:Class ;
    rdfs:label      "FoodService" ;
    dcterms:source  <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#STI_Accommodation_Ontology> ;
    rdfs:comment    "A food service, like breakfast, lunch, or dinner." ;
    rdfs:subClassOf schema:Service .

schema:Intangible
    a               rdfs:Class ;
    rdfs:label      "Intangible" ;
    rdfs:comment
                    "A utility class that serves as the umbrella for a number of 'intangible' things such as quantities, structured values, etc." ;
    rdfs:subClassOf schema:Thing .

schema:Service
    a               rdfs:Class ;
    rdfs:label      "Service" ;
    rdfs:comment    "A service provided by an organization, e.g. delivery service, print services, etc." ;
    rdfs:subClassOf schema:Intangible .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:aggregateRating
    a                     rdf:Property ;
    rdfs:label            "aggregateRating" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:AggregateRating ;
    rdfs:comment          "The overall rating, based on a collection of reviews or ratings, of the item." .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:availableChannel
    a                     rdf:Property ;
    rdfs:label            "availableChannel" ;
    schema:domainIncludes schema:Service ;
    schema:rangeIncludes  schema:ServiceChannel ;
    rdfs:comment          "A means of accessing the service (e.g. a phone bank, a web site, a location, etc.)." .

schema:award
    a                     rdf:Property ;
    rdfs:label            "award" ;
    schema:domainIncludes schema:CreativeWork, schema:Organization, schema:Person, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An award won by or for this item." .

schema:brand
    a                     rdf:Property ;
    rdfs:label            "brand" ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Brand, schema:Organization ;
    rdfs:comment
                          "The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person." .

schema:broker
    a                     rdf:Property ;
    rdfs:label            "broker" ;
    schema:domainIncludes schema:Invoice, schema:Order, schema:Reservation, schema:Service ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "An entity that arranges for an exchange between a buyer and a seller.  In most cases a broker never acquires or releases ownership of a product or service involved in an exchange.  If it is not clear whether an entity is a broker, seller, or buyer, the latter two terms are preferred." .

schema:category
    a                     rdf:Property ;
    rdfs:label            "category" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1741> ;
    schema:category       "issue-1741" ;
    schema:domainIncludes schema:ActionAccessSpecification, schema:Invoice, schema:Offer, schema:PhysicalActivity,
                          schema:Product, schema:Recommendation, schema:Service ;
    schema:rangeIncludes  schema:PhysicalActivityCategory, schema:Text, schema:Thing ;
    rdfs:comment
                          "A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:hasOfferCatalog
    a                     rdf:Property ;
    rdfs:label            "hasOfferCatalog" ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Service ;
    schema:rangeIncludes  schema:OfferCatalog ;
    rdfs:comment          "Indicates an OfferCatalog listing for this Organization, Person, or Service." .

schema:hoursAvailable
    a                     rdf:Property ;
    rdfs:label            "hoursAvailable" ;
    schema:domainIncludes schema:ContactPoint, schema:LocationFeatureSpecification, schema:Service ;
    schema:rangeIncludes  schema:OpeningHoursSpecification ;
    rdfs:comment          "The hours during which this service or contact is available." .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:isRelatedTo
    a                     rdf:Property ;
    rdfs:label            "isRelatedTo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Product, schema:Service ;
    rdfs:comment          "A pointer to another, somehow related product (or multiple products)." .

schema:isSimilarTo
    a                     rdf:Property ;
    rdfs:label            "isSimilarTo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Product, schema:Service ;
    rdfs:comment          "A pointer to another, functionally similar product (or multiple products)." .

schema:logo
    a                     rdf:Property ;
    rdfs:label            "logo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment          "An associated logo." ;
    rdfs:subPropertyOf    schema:image .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:offers
    a                     rdf:Property ;
    rdfs:label            "offers" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2289> ;
    schema:category       "issue-2289" ;
    schema:domainIncludes schema:AggregateOffer, schema:CreativeWork, schema:EducationalOccupationalProgram,
                          schema:Event, schema:MenuItem, schema:Product, schema:Service, schema:Trip ;
    schema:inverseOf      schema:itemOffered ;
    schema:rangeIncludes  schema:Demand, schema:Offer ;
    rdfs:comment
                          "An offer to provide this item&#x2014;for example, an offer to sell a product, rent the DVD of a movie, perform a service, or give away tickets to an event. Use <a class=\"localLink\" href=\"http://schema.org/businessFunction\">businessFunction</a> to indicate the kind of transaction offered, i.e. sell, lease, etc. This property can also be used to describe a <a class=\"localLink\" href=\"http://schema.org/Demand\">Demand</a>. While this property is listed as expected on a number of common types, it can be used in others. In that case, using a second type, such as Product or a subtype of Product, can clarify the nature of the offer." .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:produces
    a                     rdf:Property ;
    rdfs:label            "produces" ;
    schema:domainIncludes schema:Service ;
    schema:rangeIncludes  schema:Thing ;
    schema:supersededBy   schema:serviceOutput ;
    rdfs:comment          "The tangible thing generated by the service, e.g. a passport, permit, etc." .

schema:provider
    a                     rdf:Property ;
    rdfs:label            "provider" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2289> ;
    schema:category       "issue-2289" ;
    schema:domainIncludes schema:CreativeWork, schema:EducationalOccupationalProgram, schema:Invoice,
                          schema:ParcelDelivery, schema:Reservation, schema:Service, schema:Trip ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller." .

schema:providerMobility
    a                     rdf:Property ;
    rdfs:label            "providerMobility" ;
    schema:domainIncludes schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "Indicates the mobility of a provided service (e.g. 'static', 'dynamic')." .

schema:review
    a                     rdf:Property ;
    rdfs:label            "review" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Review ;
    rdfs:comment          "A review of the item." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:serviceArea
    a                     rdf:Property ;
    rdfs:label            "serviceArea" ;
    schema:domainIncludes schema:ContactPoint, schema:Organization, schema:Service ;
    schema:rangeIncludes  schema:AdministrativeArea, schema:GeoShape, schema:Place ;
    schema:supersededBy   schema:areaServed ;
    rdfs:comment          "The geographic area where the service is provided." .

schema:serviceAudience
    a                     rdf:Property ;
    rdfs:label            "serviceAudience" ;
    schema:domainIncludes schema:Service ;
    schema:rangeIncludes  schema:Audience ;
    schema:supersededBy   schema:audience ;
    rdfs:comment          "The audience eligible for this service." .

schema:serviceType
    a                     rdf:Property ;
    rdfs:label            "serviceType" ;
    schema:domainIncludes schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The type of service being offered, e.g. veterans' benefits, emergency relief, etc." .

schema:slogan
    a                     rdf:Property ;
    rdfs:label            "slogan" ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "A slogan or motto associated with the item." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:termsOfService
    a                     rdf:Property ;
    rdfs:label            "termsOfService" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1423> ;
    schema:category       "issue-1423" ;
    schema:domainIncludes schema:Service ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Text, schema:URL ;
    rdfs:comment          "Human-readable terms of service documentation." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:areaServed
    a                     rdf:Property ;
    rdfs:label            "areaServed" ;
    schema:domainIncludes schema:ContactPoint, schema:DeliveryChargeSpecification, schema:Demand, schema:Offer,
                          schema:Organization, schema:Service ;
    schema:rangeIncludes  schema:AdministrativeArea, schema:GeoShape, schema:Place, schema:Text ;
    rdfs:comment          "The geographic area where a service or offered item is provided." .

schema:audience
    a                     rdf:Property ;
    rdfs:label            "audience" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:LodgingBusiness, schema:PlayAction, schema:Product,
                          schema:Service ;
    schema:rangeIncludes  schema:Audience ;
    rdfs:comment          "An intended audience, i.e. a group for whom something was created." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:serviceOutput
    a                     rdf:Property ;
    rdfs:label            "serviceOutput" ;
    schema:domainIncludes schema:Service ;
    schema:rangeIncludes  schema:Thing ;
    rdfs:comment          "The tangible thing generated by the service, e.g. a passport, permit, etc." .


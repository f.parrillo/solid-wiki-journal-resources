@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:Intangible
    a               rdfs:Class ;
    rdfs:label      "Intangible" ;
    rdfs:comment
                    "A utility class that serves as the umbrella for a number of 'intangible' things such as quantities, structured values, etc." ;
    rdfs:subClassOf schema:Thing .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:ActionAccessSpecification
    rdfs:subClassOf schema:Intangible .

schema:AlignmentObject
    rdfs:subClassOf schema:Intangible .

schema:Audience
    rdfs:subClassOf schema:Intangible .

schema:BedDetails
    rdfs:subClassOf schema:Intangible .

schema:Brand
    rdfs:subClassOf schema:Intangible .

schema:BroadcastChannel
    rdfs:subClassOf schema:Intangible .

schema:BroadcastFrequencySpecification
    rdfs:subClassOf schema:Intangible .

schema:Class
    rdfs:subClassOf schema:Intangible .

schema:ComputerLanguage
    rdfs:subClassOf schema:Intangible .

schema:DataFeedItem
    rdfs:subClassOf schema:Intangible .

schema:DefinedTerm
    rdfs:subClassOf schema:Intangible .

schema:Demand
    rdfs:subClassOf schema:Intangible .

schema:DigitalDocumentPermission
    rdfs:subClassOf schema:Intangible .

schema:EducationalOccupationalProgram
    rdfs:subClassOf schema:Intangible .

schema:EntryPoint
    rdfs:subClassOf schema:Intangible .

schema:Enumeration
    rdfs:subClassOf schema:Intangible .

schema:FloorPlan
    rdfs:subClassOf schema:Intangible .

schema:GameServer
    rdfs:subClassOf schema:Intangible .

schema:GeospatialGeometry
    rdfs:subClassOf schema:Intangible .

schema:Grant
    rdfs:subClassOf schema:Intangible .

schema:HealthInsurancePlan
    rdfs:subClassOf schema:Intangible .

schema:HealthPlanCostSharingSpecification
    rdfs:subClassOf schema:Intangible .

schema:HealthPlanFormulary
    rdfs:subClassOf schema:Intangible .

schema:HealthPlanNetwork
    rdfs:subClassOf schema:Intangible .

schema:Invoice
    rdfs:subClassOf schema:Intangible .

schema:ItemList
    rdfs:subClassOf schema:Intangible .

schema:JobPosting
    rdfs:subClassOf schema:Intangible .

schema:Language
    rdfs:subClassOf schema:Intangible .

schema:ListItem
    rdfs:subClassOf schema:Intangible .

schema:MediaSubscription
    rdfs:subClassOf schema:Intangible .

schema:MenuItem
    rdfs:subClassOf schema:Intangible .

schema:MerchantReturnPolicy
    rdfs:subClassOf schema:Intangible .

schema:Observation
    rdfs:subClassOf schema:Intangible .

schema:Occupation
    rdfs:subClassOf schema:Intangible .

schema:Offer
    rdfs:subClassOf schema:Intangible .

schema:Order
    rdfs:subClassOf schema:Intangible .

schema:OrderItem
    rdfs:subClassOf schema:Intangible .

schema:ParcelDelivery
    rdfs:subClassOf schema:Intangible .

schema:Permit
    rdfs:subClassOf schema:Intangible .

schema:ProductReturnPolicy
    rdfs:subClassOf schema:Intangible .

schema:ProgramMembership
    rdfs:subClassOf schema:Intangible .

schema:Property
    rdfs:subClassOf schema:Intangible .

schema:PropertyValueSpecification
    rdfs:subClassOf schema:Intangible .

schema:Quantity
    rdfs:subClassOf schema:Intangible .

schema:Rating
    rdfs:subClassOf schema:Intangible .

schema:Reservation
    rdfs:subClassOf schema:Intangible .

schema:Role
    rdfs:subClassOf schema:Intangible .

schema:Schedule
    rdfs:subClassOf schema:Intangible .

schema:Seat
    rdfs:subClassOf schema:Intangible .

schema:Series
    rdfs:subClassOf schema:Intangible .

schema:Service
    rdfs:subClassOf schema:Intangible .

schema:ServiceChannel
    rdfs:subClassOf schema:Intangible .

schema:SpeakableSpecification
    rdfs:subClassOf schema:Intangible .

schema:StatisticalPopulation
    rdfs:subClassOf schema:Intangible .

schema:StructuredValue
    rdfs:subClassOf schema:Intangible .

schema:Ticket
    rdfs:subClassOf schema:Intangible .

schema:Trip
    rdfs:subClassOf schema:Intangible .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .


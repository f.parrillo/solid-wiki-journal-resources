@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:BusStation
    a               rdfs:Class ;
    rdfs:label      "BusStation" ;
    rdfs:comment    "A bus station." ;
    rdfs:subClassOf schema:CivicStructure .

schema:CivicStructure
    a               rdfs:Class ;
    rdfs:label      "CivicStructure" ;
    rdfs:comment    "A public structure, such as a town hall or concert hall." ;
    rdfs:subClassOf schema:Place .

schema:Place
    a               rdfs:Class ;
    rdfs:label      "Place" ;
    rdfs:comment    "Entities that have a somewhat fixed, physical extension." ;
    rdfs:subClassOf schema:Thing .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:additionalProperty
    a                     rdf:Property ;
    rdfs:label            "additionalProperty" ;
    schema:domainIncludes schema:Place, schema:Product, schema:QualitativeValue, schema:QuantitativeValue ;
    schema:rangeIncludes  schema:PropertyValue ;
    rdfs:comment          """A property-value pair representing an additional characteristics of the entitity, e.g. a product feature or another characteristic for which there is no matching property in schema.org.<br/><br/>

Note: Publishers should be aware that applications designed to use specific schema.org properties (e.g. http://schema.org/width, http://schema.org/color, http://schema.org/gtin13, ...) will typically expect such data to be provided using those properties, rather than using the generic property/value mechanism.""" .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:address
    a                     rdf:Property ;
    rdfs:label            "address" ;
    schema:domainIncludes schema:GeoCoordinates, schema:GeoShape, schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:PostalAddress, schema:Text ;
    rdfs:comment          "Physical address of the item." .

schema:aggregateRating
    a                     rdf:Property ;
    rdfs:label            "aggregateRating" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:AggregateRating ;
    rdfs:comment          "The overall rating, based on a collection of reviews or ratings, of the item." .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:amenityFeature
    a                     rdf:Property ;
    rdfs:label            "amenityFeature" ;
    dcterms:source        <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#STI_Accommodation_Ontology> ;
    schema:domainIncludes schema:Accommodation, schema:LodgingBusiness, schema:Place ;
    schema:rangeIncludes  schema:LocationFeatureSpecification ;
    rdfs:comment
                          "An amenity feature (e.g. a characteristic or service) of the Accommodation. This generic property does not make a statement about whether the feature is included in an offer for the main accommodation or available at extra costs." .

schema:arrivalBusStop
    schema:rangeIncludes schema:BusStation .

schema:branchCode
    a                     rdf:Property ;
    rdfs:label            "branchCode" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          """A short textual code (also called "store code") that uniquely identifies a place of business. The code is typically assigned by the parentOrganization and used in structured URLs.<br/><br/>

For example, in the URL http://www.starbucks.co.uk/store-locator/etc/detail/3047 the code "3047" is a branchCode for a particular branch.""" .

schema:containedIn
    a                     rdf:Property ;
    rdfs:label            "containedIn" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:Place ;
    schema:supersededBy   schema:containedInPlace ;
    rdfs:comment          "The basic containment relation between a place and one that contains it." .

schema:departureBusStop
    schema:rangeIncludes schema:BusStation .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:events
    a                     rdf:Property ;
    rdfs:label            "events" ;
    schema:domainIncludes schema:Organization, schema:Place ;
    schema:rangeIncludes  schema:Event ;
    schema:supersededBy   schema:event ;
    rdfs:comment          "Upcoming or past events associated with this place or organization." .

schema:faxNumber
    a                     rdf:Property ;
    rdfs:label            "faxNumber" ;
    schema:domainIncludes schema:ContactPoint, schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The fax number." .

schema:geo
    a                     rdf:Property ;
    rdfs:label            "geo" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:GeoCoordinates, schema:GeoShape ;
    rdfs:comment          "The geo coordinates of the place." .

schema:geoContains
    a                     rdf:Property ;
    rdfs:label            "geoContains" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents a relationship between two geometries (or the places they represent), relating a containing geometry to a contained geometry. \"a contains b iff no points of b lie in the exterior of a, and at least one point of the interior of b lies in the interior of a\". As defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>." .

schema:geoCoveredBy
    a                     rdf:Property ;
    rdfs:label            "geoCoveredBy" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents a relationship between two geometries (or the places they represent), relating a geometry to another that covers it. As defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>." .

schema:geoCovers
    a                     rdf:Property ;
    rdfs:label            "geoCovers" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents a relationship between two geometries (or the places they represent), relating a covering geometry to a covered geometry. \"Every point of b is a point of (the interior or boundary of) a\". As defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>." .

schema:geoCrosses
    a                     rdf:Property ;
    rdfs:label            "geoCrosses" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents a relationship between two geometries (or the places they represent), relating a geometry to another that crosses it: \"a crosses b: they have some but not all interior points in common, and the dimension of the intersection is less than that of at least one of them\". As defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>." .

schema:geoDisjoint
    a                     rdf:Property ;
    rdfs:label            "geoDisjoint" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents spatial relations in which two geometries (or the places they represent) are topologically disjoint: they have no point in common. They form a set of disconnected geometries.\" (a symmetric relationship, as defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>)" .

schema:geoEquals
    a                     rdf:Property ;
    rdfs:label            "geoEquals" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents spatial relations in which two geometries (or the places they represent) are topologically equal, as defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>. \"Two geometries are topologically equal if their interiors intersect and no part of the interior or boundary of one geometry intersects the exterior of the other\" (a symmetric relationship)" .

schema:geoIntersects
    a                     rdf:Property ;
    rdfs:label            "geoIntersects" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents spatial relations in which two geometries (or the places they represent) have at least one point in common. As defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>." .

schema:geoOverlaps
    a                     rdf:Property ;
    rdfs:label            "geoOverlaps" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents a relationship between two geometries (or the places they represent), relating a geometry to another that geospatially overlaps it, i.e. they have some but not all points in common. As defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>." .

schema:geoTouches
    a                     rdf:Property ;
    rdfs:label            "geoTouches" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents spatial relations in which two geometries (or the places they represent) touch: they have at least one boundary point in common, but no interior points.\" (a symmetric relationship, as defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a> )" .

schema:geoWithin
    a                     rdf:Property ;
    rdfs:label            "geoWithin" ;
    schema:domainIncludes schema:GeospatialGeometry, schema:Place ;
    schema:rangeIncludes  schema:GeospatialGeometry, schema:Place ;
    rdfs:comment
                          "Represents a relationship between two geometries (or the places they represent), relating a geometry to one that contains it, i.e. it is inside (i.e. within) its interior. As defined in <a href=\"https://en.wikipedia.org/wiki/DE-9IM\">DE-9IM</a>." .

schema:globalLocationNumber
    a                     rdf:Property ;
    rdfs:label            "globalLocationNumber" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The <a href=\"http://www.gs1.org/gln\">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations." ;
    rdfs:subPropertyOf    schema:identifier .

schema:isAccessibleForFree
    a                     rdf:Property ;
    rdfs:label            "isAccessibleForFree" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:Place, schema:PublicationEvent ;
    schema:rangeIncludes  schema:Boolean ;
    rdfs:comment          "A flag to signal that the item, event, or place is accessible for free." .

schema:isicV4
    a                     rdf:Property ;
    rdfs:label            "isicV4" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place." .

schema:latitude
    a                     rdf:Property ;
    rdfs:label            "latitude" ;
    schema:domainIncludes schema:GeoCoordinates, schema:Place ;
    schema:rangeIncludes  schema:Number, schema:Text ;
    rdfs:comment
                          "The latitude of a location. For example <code>37.42242</code> (<a href=\"https://en.wikipedia.org/wiki/World_Geodetic_System\">WGS 84</a>)." .

schema:logo
    a                     rdf:Property ;
    rdfs:label            "logo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment          "An associated logo." ;
    rdfs:subPropertyOf    schema:image .

schema:longitude
    a                     rdf:Property ;
    rdfs:label            "longitude" ;
    schema:domainIncludes schema:GeoCoordinates, schema:Place ;
    schema:rangeIncludes  schema:Number, schema:Text ;
    rdfs:comment
                          "The longitude of a location. For example <code>-122.08585</code> (<a href=\"https://en.wikipedia.org/wiki/World_Geodetic_System\">WGS 84</a>)." .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:map
    a                     rdf:Property ;
    rdfs:label            "map" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:URL ;
    schema:supersededBy   schema:hasMap ;
    rdfs:comment          "A URL to a map of the place." .

schema:maps
    a                     rdf:Property ;
    rdfs:label            "maps" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:URL ;
    schema:supersededBy   schema:hasMap ;
    rdfs:comment          "A URL to a map of the place." .

schema:maximumAttendeeCapacity
    a                     rdf:Property ;
    rdfs:label            "maximumAttendeeCapacity" ;
    schema:domainIncludes schema:Event, schema:Place ;
    schema:rangeIncludes  schema:Integer ;
    rdfs:comment          "The total number of individuals that may attend an event or venue." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:openingHours
    a                     rdf:Property ;
    rdfs:label            "openingHours" ;
    schema:domainIncludes schema:CivicStructure, schema:LocalBusiness ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          """The general opening hours for a business. Opening hours can be specified as a weekly time range, starting with days, then times per day. Multiple days can be listed with commas ',' separating each day. Day or time ranges are specified using a hyphen '-'.<br/><br/>

<ul>
<li>Days are specified using the following two-letter combinations: <code>Mo</code>, <code>Tu</code>, <code>We</code>, <code>Th</code>, <code>Fr</code>, <code>Sa</code>, <code>Su</code>.</li>
<li>Times are specified using 24:00 time. For example, 3pm is specified as <code>15:00</code>. </li>
<li>Here is an example: <code>&lt;time itemprop="openingHours" datetime=&quot;Tu,Th 16:00-20:00&quot;&gt;Tuesdays and Thursdays 4-8pm&lt;/time&gt;</code>.</li>
<li>If a business is open 7 days a week, then it can be specified as <code>&lt;time itemprop=&quot;openingHours&quot; datetime=&quot;Mo-Su&quot;&gt;Monday through Sunday, all day&lt;/time&gt;</code>.</li>
</ul>
""" .

schema:openingHoursSpecification
    a                     rdf:Property ;
    rdfs:label            "openingHoursSpecification" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:OpeningHoursSpecification ;
    rdfs:comment          "The opening hours of a certain place." .

schema:photos
    a                     rdf:Property ;
    rdfs:label            "photos" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:ImageObject, schema:Photograph ;
    schema:supersededBy   schema:photo ;
    rdfs:comment          "Photographs of this place." .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:publicAccess
    a                     rdf:Property ;
    rdfs:label            "publicAccess" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:Boolean ;
    rdfs:comment
                          "A flag to signal that the <a class=\"localLink\" href=\"http://schema.org/Place\">Place</a> is open to public visitors.  If this property is omitted there is no assumed default boolean value" .

schema:reviews
    a                     rdf:Property ;
    rdfs:label            "reviews" ;
    schema:domainIncludes schema:CreativeWork, schema:Offer, schema:Organization, schema:Place, schema:Product ;
    schema:rangeIncludes  schema:Review ;
    schema:supersededBy   schema:review ;
    rdfs:comment          "Review of the item." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:slogan
    a                     rdf:Property ;
    rdfs:label            "slogan" ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "A slogan or motto associated with the item." .

schema:smokingAllowed
    a                     rdf:Property ;
    rdfs:label            "smokingAllowed" ;
    dcterms:source        <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#STI_Accommodation_Ontology> ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:Boolean ;
    rdfs:comment
                          "Indicates whether it is allowed to smoke in the place, e.g. in the restaurant, hotel or hotel room." .

schema:specialOpeningHoursSpecification
    a                     rdf:Property ;
    rdfs:label            "specialOpeningHoursSpecification" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:OpeningHoursSpecification ;
    rdfs:comment          """The special opening hours of a certain place.<br/><br/>

Use this to explicitly override general opening hours brought in scope by <a class="localLink" href="http://schema.org/openingHoursSpecification">openingHoursSpecification</a> or <a class="localLink" href="http://schema.org/openingHours">openingHours</a>.""" .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:telephone
    a                     rdf:Property ;
    rdfs:label            "telephone" ;
    schema:domainIncludes schema:ContactPoint, schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The telephone number." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:containsPlace
    a                     rdf:Property ;
    rdfs:label            "containsPlace" ;
    schema:domainIncludes schema:Place ;
    schema:inverseOf      schema:containedInPlace ;
    schema:rangeIncludes  schema:Place ;
    rdfs:comment          "The basic containment relation between a place and another that it contains." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

schema:event
    a                     rdf:Property ;
    rdfs:label            "event" ;
    schema:domainIncludes schema:InformAction, schema:InviteAction, schema:JoinAction, schema:LeaveAction,
                          schema:Organization, schema:Place, schema:PlayAction ;
    schema:rangeIncludes  schema:Event ;
    rdfs:comment          "Upcoming or past event associated with this place, organization, or action." .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:photo
    a                     rdf:Property ;
    rdfs:label            "photo" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:ImageObject, schema:Photograph ;
    rdfs:comment          "A photograph of this place." ;
    rdfs:subPropertyOf    schema:image .

schema:review
    a                     rdf:Property ;
    rdfs:label            "review" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Review ;
    rdfs:comment          "A review of the item." .

schema:containedInPlace
    a                     rdf:Property ;
    rdfs:label            "containedInPlace" ;
    schema:domainIncludes schema:Place ;
    schema:inverseOf      schema:containsPlace ;
    schema:rangeIncludes  schema:Place ;
    rdfs:comment          "The basic containment relation between a place and one that contains it." .

schema:hasMap
    a                     rdf:Property ;
    rdfs:label            "hasMap" ;
    schema:domainIncludes schema:Place ;
    schema:rangeIncludes  schema:Map, schema:URL ;
    rdfs:comment          "A URL to a map of the place." .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .


@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:EducationalOrganization
    a               rdfs:Class ;
    rdfs:label      "EducationalOrganization" ;
    rdfs:comment    "An educational organization." ;
    rdfs:subClassOf schema:Organization .

schema:Organization
    a               rdfs:Class ;
    rdfs:label      "Organization" ;
    rdfs:comment    "An organization such as a school, NGO, corporation, club, etc." ;
    rdfs:subClassOf schema:Thing .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:CollegeOrUniversity
    rdfs:subClassOf schema:EducationalOrganization .

schema:ElementarySchool
    rdfs:subClassOf schema:EducationalOrganization .

schema:HighSchool
    rdfs:subClassOf schema:EducationalOrganization .

schema:MiddleSchool
    rdfs:subClassOf schema:EducationalOrganization .

schema:Preschool
    rdfs:subClassOf schema:EducationalOrganization .

schema:School
    rdfs:subClassOf schema:EducationalOrganization .

schema:actionableFeedbackPolicy
    a                     rdf:Property ;
    rdfs:label            "actionableFeedbackPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1525>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1525" ;
    schema:domainIncludes schema:NewsMediaOrganization, schema:Organization ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "For a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a> or other news-related <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a>, a statement about public engagement activities (for news media, the newsroom’s), including involving the public - digitally or otherwise -- in coverage decisions, reporting and activities after publication." ;
    rdfs:subPropertyOf    schema:publishingPrinciples .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:address
    a                     rdf:Property ;
    rdfs:label            "address" ;
    schema:domainIncludes schema:GeoCoordinates, schema:GeoShape, schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:PostalAddress, schema:Text ;
    rdfs:comment          "Physical address of the item." .

schema:aggregateRating
    a                     rdf:Property ;
    rdfs:label            "aggregateRating" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:AggregateRating ;
    rdfs:comment          "The overall rating, based on a collection of reviews or ratings, of the item." .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:alumni
    a                     rdf:Property ;
    rdfs:label            "alumni" ;
    schema:domainIncludes schema:EducationalOrganization, schema:Organization ;
    schema:inverseOf      schema:alumniOf ;
    schema:rangeIncludes  schema:Person ;
    rdfs:comment          "Alumni of an organization." .

schema:awards
    a                     rdf:Property ;
    rdfs:label            "awards" ;
    schema:domainIncludes schema:CreativeWork, schema:Organization, schema:Person, schema:Product ;
    schema:rangeIncludes  schema:Text ;
    schema:supersededBy   schema:award ;
    rdfs:comment          "Awards won by or for this item." .

schema:brand
    a                     rdf:Property ;
    rdfs:label            "brand" ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Brand, schema:Organization ;
    rdfs:comment
                          "The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person." .

schema:contactPoints
    a                     rdf:Property ;
    rdfs:label            "contactPoints" ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:ContactPoint ;
    schema:supersededBy   schema:contactPoint ;
    rdfs:comment          "A contact point for a person or organization." .

schema:correctionsPolicy
    a                     rdf:Property ;
    rdfs:label            "correctionsPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1525>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1525" ;
    schema:domainIncludes schema:NewsMediaOrganization, schema:Organization ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "For an <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a> (e.g. <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a>), a statement describing (in news media, the newsroom’s) disclosure and correction policy for errors." ;
    rdfs:subPropertyOf    schema:publishingPrinciples .

schema:department
    a                     rdf:Property ;
    rdfs:label            "department" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Organization ;
    rdfs:comment
                          "A relationship between an organization and a department of that organization, also described as an organization (allowing different urls, logos, opening hours). For example: a store with a pharmacy, or a bakery with a cafe." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:dissolutionDate
    a                     rdf:Property ;
    rdfs:label            "dissolutionDate" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Date ;
    rdfs:comment          "The date that this organization was dissolved." .

schema:diversityPolicy
    a                     rdf:Property ;
    rdfs:label            "diversityPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1525>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1525" ;
    schema:domainIncludes schema:NewsMediaOrganization, schema:Organization ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Statement on diversity policy by an <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a> e.g. a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a>. For a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a>, a statement describing the newsroom’s diversity policy on both staffing and sources, typically providing staffing data." .

schema:diversityStaffingReport
    a                     rdf:Property ;
    rdfs:label            "diversityStaffingReport" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1525>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1525" ;
    schema:domainIncludes schema:NewsMediaOrganization, schema:Organization ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Article, schema:URL ;
    rdfs:comment
                          "For an <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a> (often but not necessarily a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a>), a report on staffing diversity issues. In a news context this might be for example ASNE or RTDNA (US) reports, or self-reported." ;
    rdfs:subPropertyOf    schema:publishingPrinciples .

schema:duns
    a                     rdf:Property ;
    rdfs:label            "duns" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The Dun &amp; Bradstreet DUNS number for identifying an organization or business person." ;
    rdfs:subPropertyOf    schema:identifier .

schema:email
    a                     rdf:Property ;
    rdfs:label            "email" ;
    schema:domainIncludes schema:ContactPoint, schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "Email address." .

schema:employees
    a                     rdf:Property ;
    rdfs:label            "employees" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Person ;
    schema:supersededBy   schema:employee ;
    rdfs:comment          "People working for this organization." .

schema:ethicsPolicy
    a                     rdf:Property ;
    rdfs:label            "ethicsPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1525> ;
    schema:category       "issue-1525" ;
    schema:domainIncludes schema:NewsMediaOrganization, schema:Organization ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Statement about ethics policy, e.g. of a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a> regarding journalistic and publishing practices, or of a <a class=\"localLink\" href=\"http://schema.org/Restaurant\">Restaurant</a>, a page describing food source policies. In the case of a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a>, an ethicsPolicy is typically a statement describing the personal, organizational, and corporate standards of behavior expected by the organization." .

schema:events
    a                     rdf:Property ;
    rdfs:label            "events" ;
    schema:domainIncludes schema:Organization, schema:Place ;
    schema:rangeIncludes  schema:Event ;
    schema:supersededBy   schema:event ;
    rdfs:comment          "Upcoming or past events associated with this place or organization." .

schema:faxNumber
    a                     rdf:Property ;
    rdfs:label            "faxNumber" ;
    schema:domainIncludes schema:ContactPoint, schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The fax number." .

schema:founders
    a                     rdf:Property ;
    rdfs:label            "founders" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Person ;
    schema:supersededBy   schema:founder ;
    rdfs:comment          "A person who founded this organization." .

schema:foundingDate
    a                     rdf:Property ;
    rdfs:label            "foundingDate" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Date ;
    rdfs:comment          "The date that this organization was founded." .

schema:foundingLocation
    a                     rdf:Property ;
    rdfs:label            "foundingLocation" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Place ;
    rdfs:comment          "The place where the Organization was founded." .

schema:funder
    a                     rdf:Property ;
    rdfs:label            "funder" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:MonetaryGrant, schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "A person or organization that supports (sponsors) something through some kind of financial contribution." ;
    rdfs:subPropertyOf    schema:sponsor .

schema:globalLocationNumber
    a                     rdf:Property ;
    rdfs:label            "globalLocationNumber" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The <a href=\"http://www.gs1.org/gln\">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations." ;
    rdfs:subPropertyOf    schema:identifier .

schema:hasCredential
    a                     rdf:Property ;
    rdfs:label            "hasCredential" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2289> ;
    schema:category       "issue-2289" ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:EducationalOccupationalCredential ;
    rdfs:comment          "A credential awarded to the Person or Organization." .

schema:hasOfferCatalog
    a                     rdf:Property ;
    rdfs:label            "hasOfferCatalog" ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Service ;
    schema:rangeIncludes  schema:OfferCatalog ;
    rdfs:comment          "Indicates an OfferCatalog listing for this Organization, Person, or Service." .

schema:hasPOS
    a                     rdf:Property ;
    rdfs:label            "hasPOS" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Place ;
    rdfs:comment          "Points-of-Sales operated by the organization or person." .

schema:hasProductReturnPolicy
    a                     rdf:Property ;
    rdfs:label            "hasProductReturnPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2288> ;
    schema:category       "issue-2288" ;
    schema:domainIncludes schema:Organization, schema:Product ;
    schema:isPartOf       <http://attic.schema.org> ;
    schema:rangeIncludes  schema:ProductReturnPolicy ;
    schema:supersededBy   schema:hasMerchantReturnPolicy ;
    rdfs:comment          "Indicates a ProductReturnPolicy that may be applicable." .

schema:interactionStatistic
    a                     rdf:Property ;
    rdfs:label            "interactionStatistic" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2421> ;
    schema:category       "issue-2421" ;
    schema:domainIncludes schema:CreativeWork, schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:InteractionCounter ;
    rdfs:comment
                          "The number of interactions for the CreativeWork using the WebSite or SoftwareApplication. The most specific child type of InteractionCounter should be used." .

schema:isicV4
    a                     rdf:Property ;
    rdfs:label            "isicV4" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place." .

schema:knowsAbout
    a                     rdf:Property ;
    rdfs:label            "knowsAbout" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1688>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1688" ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Text, schema:Thing, schema:URL ;
    rdfs:comment
                          "Of a <a class=\"localLink\" href=\"http://schema.org/Person\">Person</a>, and less typically of an <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a>, to indicate a topic that is known about - suggesting possible expertise but not implying it. We do not distinguish skill levels here, or relate this to educational content, events, objectives or <a class=\"localLink\" href=\"http://schema.org/JobPosting\">JobPosting</a> descriptions." .

schema:knowsLanguage
    a                     rdf:Property ;
    rdfs:label            "knowsLanguage" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1688>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1688" ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:Language, schema:Text ;
    rdfs:comment
                          "Of a <a class=\"localLink\" href=\"http://schema.org/Person\">Person</a>, and less typically of an <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a>, to indicate a known language. We do not distinguish skill levels or reading/writing/speaking/signing here. Use language codes from the <a href=\"http://tools.ietf.org/html/bcp47\">IETF BCP 47 standard</a>." .

schema:legalName
    a                     rdf:Property ;
    rdfs:label            "legalName" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The official name of the organization, e.g. the registered company name." .

schema:leiCode
    a                     rdf:Property ;
    rdfs:label            "leiCode" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#FIBO>,
                          <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#GLEIF> ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "An organization identifier that uniquely identifies a legal entity as defined in ISO 17442." ;
    rdfs:subPropertyOf    schema:identifier .

schema:location
    a                     rdf:Property ;
    rdfs:label            "location" ;
    schema:domainIncludes schema:Action, schema:Event, schema:Organization ;
    schema:rangeIncludes  schema:Place, schema:PostalAddress, schema:Text ;
    rdfs:comment
                          "The location of for example where the event is happening, an organization is located, or where an action takes place." .

schema:logo
    a                     rdf:Property ;
    rdfs:label            "logo" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment          "An associated logo." ;
    rdfs:subPropertyOf    schema:image .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:makesOffer
    a                     rdf:Property ;
    rdfs:label            "makesOffer" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:inverseOf      schema:offeredBy ;
    schema:rangeIncludes  schema:Offer ;
    rdfs:comment          "A pointer to products or services offered by the organization or person." .

schema:members
    a                     rdf:Property ;
    rdfs:label            "members" ;
    schema:domainIncludes schema:Organization, schema:ProgramMembership ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    schema:supersededBy   schema:member ;
    rdfs:comment          "A member of this organization." .

schema:naics
    a                     rdf:Property ;
    rdfs:label            "naics" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The North American Industry Classification System (NAICS) code for a particular organization or business person." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:numberOfEmployees
    a                     rdf:Property ;
    rdfs:label            "numberOfEmployees" ;
    schema:domainIncludes schema:BusinessAudience, schema:Organization ;
    schema:rangeIncludes  schema:QuantitativeValue ;
    rdfs:comment          "The number of employees in an organization e.g. business." .

schema:ownershipFundingInfo
    a                     rdf:Property ;
    rdfs:label            "ownershipFundingInfo" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1525>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1525" ;
    schema:domainIncludes schema:NewsMediaOrganization, schema:Organization ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:AboutPage, schema:CreativeWork, schema:Text, schema:URL ;
    rdfs:comment
                          "For an <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a> (often but not necessarily a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a>), a description of organizational ownership structure; funding and grants. In a news/media setting, this is with particular reference to editorial independence.   Note that the <a class=\"localLink\" href=\"http://schema.org/funder\">funder</a> is also available and can be used to make basic funder information machine-readable." ;
    rdfs:subPropertyOf    schema:publishingPrinciples .

schema:owns
    a                     rdf:Property ;
    rdfs:label            "owns" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:OwnershipInfo, schema:Product ;
    rdfs:comment          "Products owned by the organization or person." .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:reviews
    a                     rdf:Property ;
    rdfs:label            "reviews" ;
    schema:domainIncludes schema:CreativeWork, schema:Offer, schema:Organization, schema:Place, schema:Product ;
    schema:rangeIncludes  schema:Review ;
    schema:supersededBy   schema:review ;
    rdfs:comment          "Review of the item." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:seeks
    a                     rdf:Property ;
    rdfs:label            "seeks" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Demand ;
    rdfs:comment          "A pointer to products or services sought by the organization or person (demand)." .

schema:serviceArea
    a                     rdf:Property ;
    rdfs:label            "serviceArea" ;
    schema:domainIncludes schema:ContactPoint, schema:Organization, schema:Service ;
    schema:rangeIncludes  schema:AdministrativeArea, schema:GeoShape, schema:Place ;
    schema:supersededBy   schema:areaServed ;
    rdfs:comment          "The geographic area where the service is provided." .

schema:slogan
    a                     rdf:Property ;
    rdfs:label            "slogan" ;
    schema:domainIncludes schema:Brand, schema:Organization, schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "A slogan or motto associated with the item." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:taxID
    a                     rdf:Property ;
    rdfs:label            "taxID" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain." ;
    rdfs:subPropertyOf    schema:identifier .

schema:telephone
    a                     rdf:Property ;
    rdfs:label            "telephone" ;
    schema:domainIncludes schema:ContactPoint, schema:Organization, schema:Person, schema:Place ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The telephone number." .

schema:unnamedSourcesPolicy
    a                     rdf:Property ;
    rdfs:label            "unnamedSourcesPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1525>,
                          <https://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#TP> ;
    schema:category       "issue-1525" ;
    schema:domainIncludes schema:NewsMediaOrganization, schema:Organization ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "For an <a class=\"localLink\" href=\"http://schema.org/Organization\">Organization</a> (typically a <a class=\"localLink\" href=\"http://schema.org/NewsMediaOrganization\">NewsMediaOrganization</a>), a statement about policy on use of unnamed sources and the decision process required." ;
    rdfs:subPropertyOf    schema:publishingPrinciples .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:vatID
    a                     rdf:Property ;
    rdfs:label            "vatID" ;
    dcterms:source        <http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_GoodRelationsTerms> ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The Value-added Tax ID of the organization or person." .

schema:alumniOf
    schema:rangeIncludes schema:EducationalOrganization .

schema:areaServed
    a                     rdf:Property ;
    rdfs:label            "areaServed" ;
    schema:domainIncludes schema:ContactPoint, schema:DeliveryChargeSpecification, schema:Demand, schema:Offer,
                          schema:Organization, schema:Service ;
    schema:rangeIncludes  schema:AdministrativeArea, schema:GeoShape, schema:Place, schema:Text ;
    rdfs:comment          "The geographic area where a service or offered item is provided." .

schema:award
    a                     rdf:Property ;
    rdfs:label            "award" ;
    schema:domainIncludes schema:CreativeWork, schema:Organization, schema:Person, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An award won by or for this item." .

schema:contactPoint
    a                     rdf:Property ;
    rdfs:label            "contactPoint" ;
    schema:domainIncludes schema:HealthInsurancePlan, schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:ContactPoint ;
    rdfs:comment          "A contact point for a person or organization." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

schema:employee
    a                     rdf:Property ;
    rdfs:label            "employee" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Person ;
    rdfs:comment          "Someone working for this organization." .

schema:event
    a                     rdf:Property ;
    rdfs:label            "event" ;
    schema:domainIncludes schema:InformAction, schema:InviteAction, schema:JoinAction, schema:LeaveAction,
                          schema:Organization, schema:Place, schema:PlayAction ;
    schema:rangeIncludes  schema:Event ;
    rdfs:comment          "Upcoming or past event associated with this place, organization, or action." .

schema:founder
    a                     rdf:Property ;
    rdfs:label            "founder" ;
    schema:domainIncludes schema:Organization ;
    schema:rangeIncludes  schema:Person ;
    rdfs:comment          "A person who founded this organization." .

schema:hasMerchantReturnPolicy
    a                     rdf:Property ;
    rdfs:label            "hasMerchantReturnPolicy" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/2288> ;
    schema:category       "issue-2288" ;
    schema:domainIncludes schema:Organization, schema:Product ;
    schema:isPartOf       <http://pending.schema.org> ;
    schema:rangeIncludes  schema:MerchantReturnPolicy ;
    rdfs:comment          "Indicates a MerchantReturnPolicy that may be applicable." .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:memberOf
    a                     rdf:Property ;
    rdfs:label            "memberOf" ;
    schema:domainIncludes schema:Organization, schema:Person ;
    schema:inverseOf      schema:member ;
    schema:rangeIncludes  schema:Organization, schema:ProgramMembership ;
    rdfs:comment          "An Organization (or ProgramMembership) to which this Person or Organization belongs." .

schema:parentOrganization
    a                     rdf:Property ;
    rdfs:label            "parentOrganization" ;
    schema:domainIncludes schema:Organization ;
    schema:inverseOf      schema:subOrganization ;
    schema:rangeIncludes  schema:Organization ;
    rdfs:comment
                          "The larger organization that this organization is a <a class=\"localLink\" href=\"http://schema.org/subOrganization\">subOrganization</a> of, if any." .

schema:review
    a                     rdf:Property ;
    rdfs:label            "review" ;
    schema:domainIncludes schema:Brand, schema:CreativeWork, schema:Event, schema:Offer, schema:Organization,
                          schema:Place, schema:Product, schema:Service ;
    schema:rangeIncludes  schema:Review ;
    rdfs:comment          "A review of the item." .

schema:sponsor
    a                     rdf:Property ;
    rdfs:label            "sponsor" ;
    schema:domainIncludes schema:CreativeWork, schema:Event, schema:Grant, schema:MedicalStudy, schema:Organization,
                          schema:Person ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "A person or organization that supports a thing through a pledge, promise, or financial contribution. e.g. a sponsor of a Medical Study or a corporate sponsor of an event." .

schema:subOrganization
    a                     rdf:Property ;
    rdfs:label            "subOrganization" ;
    schema:domainIncludes schema:Organization ;
    schema:inverseOf      schema:parentOrganization ;
    schema:rangeIncludes  schema:Organization ;
    rdfs:comment
                          "A relationship between two organizations where the first includes the second, e.g., as a subsidiary. See also: the more specific 'department' property." .

schema:member
    a                     rdf:Property ;
    rdfs:label            "member" ;
    schema:domainIncludes schema:Organization, schema:ProgramMembership ;
    schema:inverseOf      schema:memberOf ;
    schema:rangeIncludes  schema:Organization, schema:Person ;
    rdfs:comment
                          "A member of an Organization or a ProgramMembership. Organizations can be members of organizations; ProgramMembership is typically for individuals." .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:publishingPrinciples
    a                     rdf:Property ;
    rdfs:label            "publishingPrinciples" ;
    schema:domainIncludes schema:CreativeWork, schema:Organization, schema:Person ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment          """The publishingPrinciples property indicates (typically via <a class="localLink" href="http://schema.org/URL">URL</a>) a document describing the editorial principles of an <a class="localLink" href="http://schema.org/Organization">Organization</a> (or individual e.g. a <a class="localLink" href="http://schema.org/Person">Person</a> writing a blog) that relate to their activities as a publisher, e.g. ethics or diversity policies. When applied to a <a class="localLink" href="http://schema.org/CreativeWork">CreativeWork</a> (e.g. <a class="localLink" href="http://schema.org/NewsArticle">NewsArticle</a>) the principles are those of the party primarily responsible for the creation of the <a class="localLink" href="http://schema.org/CreativeWork">CreativeWork</a>.<br/><br/>

While such policies are most typically expressed in natural language, sometimes related information (e.g. indicating a <a class="localLink" href="http://schema.org/funder">funder</a>) can be expressed using schema.org terminology.""" .


@prefix schema:  <http://schema.org/> .
@prefix bibo:    <http://purl.org/ontology/bibo/> .
@prefix dc:      <http://purl.org/dc/elements/1.1/> .
@prefix dcat:    <http://www.w3.org/ns/dcat#> .
@prefix dct:     <http://purl.org/dc/terms/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype:  <http://purl.org/dc/dcmitype/> .
@prefix eli:     <http://data.europa.eu/eli/ontology#> .
@prefix foaf:    <http://xmlns.com/foaf/0.1/> .
@prefix owl:     <http://www.w3.org/2002/07/owl#> .
@prefix rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfa:    <http://www.w3.org/ns/rdfa#> .
@prefix rdfs:    <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema:  <http://schema.org/> .
@prefix skos:    <http://www.w3.org/2004/02/skos/core#> .
@prefix snomed:  <http://purl.bioontology.org/ontology/SNOMEDCT/> .
@prefix void:    <http://rdfs.org/ns/void#> .
@prefix xml:     <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:     <http://www.w3.org/2001/XMLSchema#> .

schema:AnatomicalStructure
    a               rdfs:Class ;
    rdfs:label      "AnatomicalStructure" ;
    schema:isPartOf <http://health-lifesci.schema.org> ;
    rdfs:comment
                    "Any part of the human body, typically a component of an anatomical system. Organs, tissues, and cells are all anatomical structures." ;
    rdfs:subClassOf schema:MedicalEntity .

schema:Joint
    a               rdfs:Class ;
    rdfs:label      "Joint" ;
    schema:isPartOf <http://health-lifesci.schema.org> ;
    rdfs:comment    "The anatomical location at which two or more bones make contact." ;
    rdfs:subClassOf schema:AnatomicalStructure .

schema:MedicalEntity
    a               rdfs:Class ;
    rdfs:label      "MedicalEntity" ;
    schema:isPartOf <http://health-lifesci.schema.org> ;
    rdfs:comment    "The most generic type of entity related to health and the practice of medicine." ;
    rdfs:subClassOf schema:Thing .

schema:Thing
    a            rdfs:Class ;
    rdfs:label   "Thing" ;
    rdfs:comment "The most generic type of item." .

schema:additionalType
    a                     rdf:Property ;
    rdfs:label            "additionalType" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally." ;
    rdfs:subPropertyOf    rdf:type .

schema:alternateName
    a                     rdf:Property ;
    rdfs:label            "alternateName" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "An alias for the item." .

schema:associatedPathophysiology
    a                     rdf:Property ;
    rdfs:label            "associatedPathophysiology" ;
    schema:domainIncludes schema:AnatomicalStructure, schema:AnatomicalSystem, schema:SuperficialAnatomy ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "If applicable, a description of the pathophysiology associated with the anatomical system, including potential abnormal changes in the mechanical, physical, and biochemical functions of the system." .

schema:biomechnicalClass
    a                     rdf:Property ;
    rdfs:label            "biomechnicalClass" ;
    schema:domainIncludes schema:Joint ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The biomechanical properties of the bone." .

schema:bodyLocation
    a                     rdf:Property ;
    rdfs:label            "bodyLocation" ;
    schema:domainIncludes schema:AnatomicalStructure, schema:MedicalProcedure ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "Location in the body of the anatomical structure." .

schema:code
    a                     rdf:Property ;
    rdfs:label            "code" ;
    schema:domainIncludes schema:MedicalEntity ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicalCode ;
    rdfs:comment
                          "A medical code for the entity, taken from a controlled vocabulary or ontology such as ICD-9, DiseasesDB, MeSH, SNOMED-CT, RxNorm, etc." .

schema:connectedTo
    a                     rdf:Property ;
    rdfs:label            "connectedTo" ;
    schema:domainIncludes schema:AnatomicalStructure ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:AnatomicalStructure ;
    rdfs:comment          "Other anatomical structures to which this structure is connected." .

schema:diagram
    a                     rdf:Property ;
    rdfs:label            "diagram" ;
    schema:domainIncludes schema:AnatomicalStructure ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:ImageObject ;
    rdfs:comment
                          "An image containing a diagram that illustrates the structure and/or its component substructures and/or connections with other structures." .

schema:disambiguatingDescription
    a                     rdf:Property ;
    rdfs:label            "disambiguatingDescription" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment
                          "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation." ;
    rdfs:subPropertyOf    schema:description .

schema:function
    a                     rdf:Property ;
    rdfs:label            "function" ;
    schema:domainIncludes schema:AnatomicalStructure ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "Function of the anatomical structure." .

schema:functionalClass
    a                     rdf:Property ;
    rdfs:label            "functionalClass" ;
    schema:domainIncludes schema:Joint ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicalEntity, schema:Text ;
    rdfs:comment          "The degree of mobility the joint allows." .

schema:guideline
    a                     rdf:Property ;
    rdfs:label            "guideline" ;
    schema:domainIncludes schema:MedicalEntity ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicalGuideline ;
    rdfs:comment          "A medical guideline related to this entity." .

schema:identifier
    a                      rdf:Property ;
    rdfs:label             "identifier" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:PropertyValue, schema:Text, schema:URL ;
    rdfs:comment
                           "The identifier property represents any kind of identifier for any kind of <a class=\"localLink\" href=\"http://schema.org/Thing\">Thing</a>, such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See <a href=\"/docs/datamodel.html#identifierBg\">background notes</a> for more details." ;
    owl:equivalentProperty dcterms:identifier .

schema:image
    a                     rdf:Property ;
    rdfs:label            "image" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:ImageObject, schema:URL ;
    rdfs:comment
                          "An image of the item. This can be a <a class=\"localLink\" href=\"http://schema.org/URL\">URL</a> or a fully described <a class=\"localLink\" href=\"http://schema.org/ImageObject\">ImageObject</a>." .

schema:legalStatus
    a                     rdf:Property ;
    rdfs:label            "legalStatus" ;
    schema:domainIncludes schema:DietarySupplement, schema:Drug, schema:MedicalEntity ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:DrugLegalStatus, schema:MedicalEnumeration, schema:Text ;
    rdfs:comment
                          "The drug or supplement's legal status, including any controlled substance schedules that apply." .

schema:mainEntityOfPage
    a                     rdf:Property ;
    rdfs:label            "mainEntityOfPage" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:mainEntity ;
    schema:rangeIncludes  schema:CreativeWork, schema:URL ;
    rdfs:comment
                          "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See <a href=\"/docs/datamodel.html#mainEntityBackground\">background notes</a> for details." .

schema:medicineSystem
    a                     rdf:Property ;
    rdfs:label            "medicineSystem" ;
    schema:domainIncludes schema:MedicalEntity ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicineSystem ;
    rdfs:comment
                          "The system of medicine that includes this MedicalEntity, for example 'evidence-based', 'homeopathic', 'chiropractic', etc." .

schema:name
    a                      rdf:Property ;
    rdfs:label             "name" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "The name of the item." ;
    rdfs:subPropertyOf     rdfs:label ;
    owl:equivalentProperty dcterms:title .

schema:partOfSystem
    a                     rdf:Property ;
    rdfs:label            "partOfSystem" ;
    schema:domainIncludes schema:AnatomicalStructure ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:AnatomicalSystem ;
    rdfs:comment          "The anatomical or organ system that this structure is part of." .

schema:potentialAction
    a                     rdf:Property ;
    rdfs:label            "potentialAction" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:Action ;
    rdfs:comment
                          "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role." .

schema:recognizingAuthority
    a                     rdf:Property ;
    rdfs:label            "recognizingAuthority" ;
    schema:domainIncludes schema:MedicalEntity ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:Organization ;
    rdfs:comment
                          "If applicable, the organization that officially recognizes this entity as part of its endorsed system of medicine." .

schema:relatedCondition
    a                     rdf:Property ;
    rdfs:label            "relatedCondition" ;
    schema:domainIncludes schema:AnatomicalStructure, schema:AnatomicalSystem, schema:SuperficialAnatomy ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicalCondition ;
    rdfs:comment          "A medical condition associated with this anatomy." .

schema:relatedTherapy
    a                     rdf:Property ;
    rdfs:label            "relatedTherapy" ;
    schema:domainIncludes schema:AnatomicalStructure, schema:AnatomicalSystem, schema:SuperficialAnatomy ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicalTherapy ;
    rdfs:comment          "A medical therapy related to this anatomy." .

schema:relevantSpecialty
    a                     rdf:Property ;
    rdfs:label            "relevantSpecialty" ;
    schema:domainIncludes schema:MedicalEntity ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicalSpecialty ;
    rdfs:comment          "If applicable, a medical specialty in which this entity is relevant." .

schema:sameAs
    a                     rdf:Property ;
    rdfs:label            "sameAs" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment
                          "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website." .

schema:structuralClass
    a                     rdf:Property ;
    rdfs:label            "structuralClass" ;
    schema:domainIncludes schema:Joint ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:Text ;
    rdfs:comment          "The name given to how bone physically connects to each other." .

schema:study
    a                     rdf:Property ;
    rdfs:label            "study" ;
    schema:domainIncludes schema:MedicalEntity ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:MedicalStudy ;
    rdfs:comment          "A medical study or trial related to this entity." .

schema:subStructure
    a                     rdf:Property ;
    rdfs:label            "subStructure" ;
    schema:domainIncludes schema:AnatomicalStructure ;
    schema:isPartOf       <http://health-lifesci.schema.org> ;
    schema:rangeIncludes  schema:AnatomicalStructure ;
    rdfs:comment          "Component (sub-)structure(s) that comprise this anatomical structure." .

schema:subjectOf
    a                     rdf:Property ;
    rdfs:label            "subjectOf" ;
    dcterms:source        <https://github.com/schemaorg/schemaorg/issues/1670> ;
    schema:category       "issue-1670" ;
    schema:domainIncludes schema:Thing ;
    schema:inverseOf      schema:about ;
    schema:rangeIncludes  schema:CreativeWork, schema:Event ;
    rdfs:comment          "A CreativeWork or Event about this Thing." .

schema:url
    a                     rdf:Property ;
    rdfs:label            "url" ;
    schema:domainIncludes schema:Thing ;
    schema:rangeIncludes  schema:URL ;
    rdfs:comment          "URL of the item." .

schema:description
    a                      rdf:Property ;
    rdfs:label             "description" ;
    schema:domainIncludes  schema:Thing ;
    schema:rangeIncludes   schema:Text ;
    rdfs:comment           "A description of the item." ;
    owl:equivalentProperty dcterms:description .

